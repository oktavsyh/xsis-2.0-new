package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xa.miniproject.models.M_Address;
import xa.miniproject.models.M_Biodata;
import xa.miniproject.repositories.R_Address;
import xa.miniproject.repositories.R_Biodata;

import java.util.List;

@RestController
@RequestMapping(value = "/api/tambahpelamar")
@CrossOrigin(origins = "*")
public class ApiAddress {

    @Autowired
    private R_Address addrepo;
    @Autowired
    private R_Biodata biorepo;

    @GetMapping(value = "/")
    public List<M_Address> list() {
        return this.addrepo.findAll();
    }

    @GetMapping(value = "/{id}")
    public M_Address listById(@PathVariable("id") Long id) {
        return this.addrepo.findById(id).orElse(null);
    }

    @PostMapping(value = "/", consumes = "application/json")
    public M_Address save(@RequestBody M_Address add) {
        return this.addrepo.save(add);
    }

    @GetMapping(value = "/apimax")
    public Long maxid() {
        return this.biorepo.findBymaxid();
    }

    @GetMapping(value = "/getbio")
    public List<M_Biodata> listbio() {
        return this.biorepo.findAll();
    }

    @GetMapping(value = "/getbio/{id}")
    public M_Biodata getbiobyid(@PathVariable("id") Long id) {
        return this.biorepo.findById(id).orElse(null);
    }

    @PostMapping(value = "/addbio", consumes = "application/json")
    public M_Biodata save(@RequestBody M_Biodata add) {
        return this.biorepo.save(add);
    }

    // @GetMapping(value="/{phone_number1}/{phone_number2}/{email}/{identity_no}/{parent_phone_number}")
    // public Long validate100(@PathVariable("phone_number1") String p1,
    // @PathVariable("phone_number2") String p2,
    // @PathVariable("email") String p3,
    // @PathVariable("identity_no") String p4,
    // @PathVariable("parent_phone_number") String p5)
    // {
    // return this.biorepo.validate100(p1, p2, p3, p4, p5);
    // }

    @GetMapping(value = "/validate0/{identity_no}/{identity_type_id}")
    public Long validate0(@PathVariable("identity_no") String p4, @PathVariable("identity_type_id") Long p5) {
        return this.biorepo.validate0(p4, p5);
    }

    @GetMapping(value = "/validate/{email}")
    public Long validate(@PathVariable("email") String p1) {
        return this.biorepo.validate(p1);
    }

    @GetMapping(value = "/validate1/{phone_number1}")
    public Long validate1(@PathVariable("phone_number1") String p2) {
        return this.biorepo.validate1(p2);
    }

    @GetMapping(value = "/validate2/{phone_number2}")
    public Long validate2(@PathVariable("phone_number2") String p3) {
        return this.biorepo.validate2(p3);
    }

    @GetMapping(value = "/validate3/{parent_phone_number}")
    public Long validate3(@PathVariable("parent_phone_number") String p4) {
        return this.biorepo.validate3(p4);
    }

    // @GetMapping(value="/validate4/{identity_no}")
    // public Long validate4(@PathVariable("identity_no") String p5)
    // {
    // return this.biorepo.validate4(p5);
    // }
    @GetMapping(value = "/getaddress/{id}")
    public List<M_Address> getaddress(@PathVariable("id") Long id) {
        return this.addrepo.findAddByBioId(id);
    }

    @PutMapping(value = "/editbio/{id}")
    public M_Biodata editbio(@RequestBody M_Biodata item) {
        return this.biorepo.save(item);
    }

    @PutMapping(value = "/editadd/{id}")
    public M_Address editadd(@RequestBody M_Address item) {
        return this.addrepo.save(item);
    }
}
