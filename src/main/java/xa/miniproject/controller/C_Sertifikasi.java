package xa.miniproject.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import xa.miniproject.models.M_Sertifikasi;
import xa.miniproject.repositories.R_Keluarga;
import xa.miniproject.repositories.R_Sertifikasi;

@Controller
@RequestMapping("/D_Sertifikasi/")
public class C_Sertifikasi {

    @Autowired
    private R_Sertifikasi r_Sertifikasi;

    @Autowired
    private R_Keluarga r_Keluarga;

    // Lihat Sertifikasi
    @GetMapping("lihat_sertifikasi/{id}/{id2}")
    public ModelAndView lihat_sertifikasi(@PathVariable("id") Long id, @PathVariable("id2") Boolean id2) {
        ModelAndView view_sertifikasi = new ModelAndView("D_Sertifikasi/lihat_sertifikasi");

        List<M_Sertifikasi> list_sertifikasi = this.r_Sertifikasi.findByBioId(id, id2);

        view_sertifikasi.addObject("sertifikasiid", id);
        view_sertifikasi.addObject("listsertifikasi", list_sertifikasi);
        return view_sertifikasi;
    }

    // Form Tambah Sertifikasi
    @GetMapping("form_tambah_sertifikasi")
    public ModelAndView form_tambah_sertifikasi() {
        ModelAndView view_sertifikasi = new ModelAndView("D_Sertifikasi/form_tambah_sertifikasi");
        view_sertifikasi.addObject("objsertifikasi", new M_Sertifikasi());

        return view_sertifikasi;
    }

    // Controller Untuk Save Setelah Form Tambah Sertifikasi
    @PostMapping("save")
    public ModelAndView save(@ModelAttribute M_Sertifikasi sertifikasi, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            sertifikasi.setCreatedBy(new Long(10));
            sertifikasi.setCreatedOn(new Date());
            sertifikasi.setIsDelete(false);
            this.r_Sertifikasi.save(sertifikasi);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    // Form Edit #Tahap 1
    @GetMapping("edit/{id}")
    public ModelAndView edit_sertifikasi(@PathVariable("id") Long id) {
        ModelAndView view_sertifikasi = new ModelAndView("D_Sertifikasi/form_edit_sertifikasi");
        M_Sertifikasi item = this.r_Sertifikasi.findById(id).orElse(null);
        view_sertifikasi.addObject("objsertifikasi", item);

        List<M_Sertifikasi> listsertifikasi = this.r_Sertifikasi.findAll();
        view_sertifikasi.addObject("listsertifikasi", listsertifikasi);

        return view_sertifikasi;
    }

    // Form Edit Sertifikasi #Tahap 2
    @GetMapping("form_edit_sertifikasi")
    public ModelAndView form_edit_sertifikasi() {
        ModelAndView view_sertifikasi = new ModelAndView("D_Sertifikasi/form_edit_sertifikasi");
        view_sertifikasi.addObject("objsertifikasi", new M_Sertifikasi());

        return view_sertifikasi;
    }

    // Controller Untuk Save Setelah Form Edit Sertifikasi #Tahap 3
    @PostMapping("save_edit")
    public ModelAndView save_edit(@ModelAttribute M_Sertifikasi sertifikasi, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            sertifikasi.setCreatedBy(new Long(10));
            sertifikasi.setModifiedBy(new Long(10));
            sertifikasi.setModifiedOn(new Date());
            sertifikasi.setIsDelete(false);
            this.r_Sertifikasi.save(sertifikasi);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    // Hapus #Tahap 1
    @GetMapping("hapus/{id}")
    public ModelAndView hapus_sertifikasi(@PathVariable("id") Long id) {
        ModelAndView view_sertifikasi = new ModelAndView("D_Sertifikasi/form_hapus_sertifikasi");
        M_Sertifikasi item = this.r_Sertifikasi.findById(id).orElse(null);
        view_sertifikasi.addObject("objsertifikasi", item);

        List<M_Sertifikasi> listsertifikasi = this.r_Sertifikasi.findAll();
        view_sertifikasi.addObject("listsertifikasi", listsertifikasi);

        return view_sertifikasi;
    }

    // Hapus Sertifikasi #Tahap 2
    @GetMapping("form_hapus_sertifikasi")
    public ModelAndView form_hapus_sertifikasi() {
        ModelAndView view_sertifikasi = new ModelAndView("D_Sertifikasi/form_hapus_sertifikasi");
        view_sertifikasi.addObject("objsertifikasi", new M_Sertifikasi());

        return view_sertifikasi;
    }

    // is_delete #Tahap 3 Untuk Hapus tanpa menghilangkan row DB
    @PostMapping("is_delete")
    public ModelAndView is_delete(@ModelAttribute M_Sertifikasi sertifikasi, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            sertifikasi.setCreatedBy(new Long(10));
            sertifikasi.setDeletedOn(new Date());
            sertifikasi.setDeletedBy(new Long(10));
            sertifikasi.setIsDelete(true);
            this.r_Sertifikasi.save(sertifikasi);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

}