package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.*;
import xa.miniproject.repositories.*;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/D_Reschedule/")
public class C_Reschedule {
    @Autowired
    private R_Biodata r_biodata;

    @Autowired
    private R_Undangan r_undangan;

    @Autowired
    private R_UndanganDetail r_undanganDetail;

    @Autowired
    private R_Schedule_Type r_schedule_type;

    @Autowired
    private R_RO r_ro;

    @Autowired
    private R_TRO r_tro;

    // Get Undangan Detail
    @GetMapping(value = "getUndDetail/{id}")
    public M_UndanganDetail getRd(@PathVariable("id") Long id) {
        return this.r_undanganDetail.findById(id).orElse(null);
    }

    // Get Undangan
    @GetMapping(value = "getUnd/{id}")
    public M_Undangan getUnd(@PathVariable("id") Long id) {
        return this.r_undangan.findById(id).orElse(null);
    }

    // Edit Undangan
    @PutMapping(value = "editUnd/{id}")
    public M_Undangan editUnd(@RequestBody M_Undangan item) {
        return this.r_undangan.save(item);
    }


    // Edit Undangan Detail
    @PutMapping(value = "editUndDetail/{id}")
    public M_UndanganDetail editUndDetail(@RequestBody M_UndanganDetail item) {
        return this.r_undanganDetail.save(item);
    }

    // Tambah Undangan
    @PostMapping(value = "saveUnd")
    public M_Undangan saveUnd(@RequestBody M_Undangan item) {
        return this.r_undangan.save(item);
    }

    @GetMapping(value = "/maxid/")
    public Long maxid(){
        return this.r_undangan.findLastId();
    }

    @GetMapping(value = "/countdata/{id}")
    public Long countdata(@PathVariable("id") Long id){ return this.r_undanganDetail.findjumlahdata(id);}

    @GetMapping(value = "lihat_reschedule")
    public ModelAndView lihat_reschedule() {
        ModelAndView view = new ModelAndView("/D_Reschedule/lihat_reschedule");
        List<M_UndanganDetail> listjadwalulang = this.r_undanganDetail.findAll();
        view.addObject("listjadwalulang", listjadwalulang);
        return view;
    }

    @GetMapping(value = "form_edit_reschedule/{id}")
    public ModelAndView form_edit_reschedule(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("/D_Reschedule/form_edit_reschedule");

        M_UndanganDetail item = this.r_undanganDetail.findById(id).orElse(null);

        M_Undangan itemUndangan = this.r_undangan.findById(id).orElse(null);

        M_Schedule_Type itemST = this.r_schedule_type.findById(id).orElse(null);

        List<M_UndanganDetail> listundangandetail = this.r_undanganDetail.findAll();
        view.addObject("listundangandetail", listundangandetail);

        List<M_Undangan> listundangan = this.r_undangan.findAll();
        view.addObject("listundangan", listundangan);

        List<M_Schedule_Type> listst = this.r_schedule_type.findAll();
        view.addObject("listst", listst);

        List<M_Biodata> listbio = this.r_biodata.findAll();
        view.addObject("listbio", listbio);

        List<M_RO> listro = this.r_ro.findAll();
        view.addObject("listro", listro);

        List<M_TRO> listtro = this.r_tro.findAll();
        view.addObject("listtro", listtro);

        view.addObject("formubahjadwal", item);
        return view;
    }
}
