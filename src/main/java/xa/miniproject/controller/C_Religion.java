package xa.miniproject.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xa.miniproject.models.M_Religion;
import xa.miniproject.repositories.R_Religion;

/**
 * C_Religion
 */
@Controller
@RequestMapping("religion")
public class C_Religion {
 @Autowired
    private R_Religion reReligion;
    @GetMapping("lihatreligion")
    public ModelAndView lihatreligion() {
        ModelAndView view = new ModelAndView("D_Religion/lihatreligion");
        List <M_Religion> religions = this.reReligion.showreligion();
        view.addObject("religions", religions);
        return view;
    }

    @GetMapping("formtambahreligion")
    public ModelAndView formaddreligion(){
        ModelAndView view = new ModelAndView("D_Religion/formtambahreligion");
        view.addObject("religions", new M_Religion());
        return view;

    }

    @PostMapping("save")
    public ModelAndView savereligion(@ModelAttribute M_Religion religion, BindingResult result){
        if (result.hasErrors()) {
            return new ModelAndView("redirect:religion/formtambahreligion");
        } else {
            religion.setCreatedOn(new Date());
            religion.setCreatedBy(new Long(1));
            religion.setIsDelete(false);
            this.reReligion.save(religion);
            return new ModelAndView("redirect:/religion/lihatreligion");
        }

    }

    @GetMapping("edit/{id}")
    public ModelAndView showEditform(@PathVariable("id") long id){
        ModelAndView view = new ModelAndView("D_Religion/formtambahreligion");
        M_Religion religion = this.reReligion.findById(id).orElse(null);
        view.addObject("religions", religion);
        return view;
    }

    

    @GetMapping("delete/{id}")
    public ModelAndView deletereligion(@PathVariable("id") long id){
        ModelAndView view = new ModelAndView("D_Religion/deletereligion");
        M_Religion religion = this.reReligion.findById(id).orElse(null);
        view.addObject("religion", religion);
        return view;
    }

    @PostMapping(value = "delete")
    public ModelAndView softdelete(@ModelAttribute M_Religion religion, BindingResult result){
        if (result.hasErrors()) {
            return new ModelAndView("redirect:/delete/{id}");
        } else {
          
            religion.setIsDelete(true);
            religion.setDeletedBy(new Long(1));
            religion.setDeletedOn(new Date());
            this.reReligion.save(religion);
            return new ModelAndView("redirect:/religion/lihatreligion");
        }
}
}