package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_Biodata;
import xa.miniproject.models.M_Keahlian;
import xa.miniproject.models.M_SkillLevel;
import xa.miniproject.repositories.R_Biodata;
import xa.miniproject.repositories.R_Keahlian;
import xa.miniproject.repositories.R_SkillLevel;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/D_Keahlian/")
public class C_Keahlian {
    @Autowired
    private R_Keahlian krepo;

    @Autowired
    private R_SkillLevel skrepo;

    @Autowired
    private R_Biodata biorepo;

    @GetMapping(value = "view_keahlian/{id}")
    public ModelAndView view_keahlian(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("D_Keahlian/view_keahlian");
        List<M_Keahlian> ahlilist = this.krepo.findByBioId(id);
        List<M_SkillLevel> listskill = this.skrepo.findAll();
        view.addObject("ahlilist", ahlilist);
        view.addObject("listskill", listskill);
        view.addObject("bioid", id);
        view.addObject("keahlian", new M_Keahlian());
        return view;
    } //buat jadi find by keahlian id

    /*@GetMapping("view_keahlian")
    public ModelAndView view_keahlian() {
        ModelAndView view = new ModelAndView("D_Keahlian/view_keahlian");
        List<M_Keahlian> ahlilist = this.krepo.findAll();
        view.addObject("ahlilist", ahlilist);
        return view;
    }*/

    @GetMapping(value = "form_keahlian")
    public ModelAndView form_keahlian(){
        ModelAndView view = new ModelAndView("D_Keahlian/form_keahlian");
        view.addObject("keahlian", new M_Keahlian());

        List<M_SkillLevel> listskill = this.skrepo.findAll();
        view.addObject("listskill", listskill);

        List<M_Biodata> biolist = this.biorepo.findAll();
        view.addObject("biolist", biolist);

        return view;
    }

    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("D_Keahlian/edit_keahlian");
        M_Keahlian keahlian = this.krepo.findById(id)
                .orElseThrow(()->
                        new IllegalArgumentException("Invalid Keahlian ID :"+id));

        view.addObject("keahlian", keahlian);
        List<M_SkillLevel> listskill = this.skrepo.findAll();
        view.addObject("listskill", listskill);
        return view;
    }

    @GetMapping(value = "edit_keahlian")
    public ModelAndView edit_keahlian(){
        ModelAndView view = new ModelAndView("D_Keahlian/edit_keahlian");
        view.addObject("keahlian", new M_Keahlian());

        List<M_SkillLevel> listskill = this.skrepo.findAll();
        view.addObject("listskill", listskill);

        List<M_Biodata> biolist = this.biorepo.findAll();
        view.addObject("biolist", biolist);

        return view;
    }

    @GetMapping(value = "delete_update/{id}")
    public ModelAndView delete_update(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("D_Keahlian/delete_update");
        M_Keahlian item = this.krepo.findById(id).orElse(null);
        view.addObject("keahlian", item);
        return view;
    }

    @GetMapping(value = "/getKeahlian/{id}")
    public M_Keahlian getKeahlian(@PathVariable("id") Long id){ return this.krepo.findById(id).orElse(null);}

    @PostMapping(value = "/saveKeahlian/")
    public M_Keahlian saveKeahlian(@RequestBody M_Keahlian item){
        return this.krepo.save(item);
    }

    @PutMapping(value = "/editKeahlian/{id}")
    public M_Keahlian editKeahlian(@RequestBody M_Keahlian item){
        return this.krepo.save(item);
    }
}
