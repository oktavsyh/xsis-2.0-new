package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_Role;
import xa.miniproject.repositories.R_Role;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/D_Role/")
public class C_Role {
    @Autowired
    private R_Role r_role;

    @GetMapping(value = "lihat_role")
    public ModelAndView view(){
        ModelAndView view = new ModelAndView("/D_Role/lihat_role");
        List<M_Role> roleList = this.r_role.findByBioId();
        view.addObject("roleList",roleList);
        return view;
    }

    @GetMapping(value = "form")
    public ModelAndView form(){
        ModelAndView form = new ModelAndView("/D_Role/form");
        form.addObject("objrole",new M_Role());
        return form;
    }

    @PostMapping(value = "save")
    public ModelAndView save(@ModelAttribute M_Role role , BindingResult result){
        if(result.hasErrors()){
            return new ModelAndView("redirect:/D_Role/form");
        }
        else{
            role.setCreatedBy(new Long(1));
            role.setCreatedOn(new Date());
            role.setDelete(false);
            this.r_role.save(role);
            return new ModelAndView("redirect:/D_Role/lihat_role");
        }
    }

    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_Role/form_edit");
        M_Role item = this.r_role.findById(id).orElse(null);
        view.addObject("objrole",item);
        return view;
    }

    @PostMapping(value = "save_edit")
    public ModelAndView save_edit(@ModelAttribute M_Role role , BindingResult result){
        if(result.hasErrors()){
            return new ModelAndView("redirect:/D_Role/form_edit");
        }
        else{
            role.setModifiedBy(new Long(1));
            role.setModifiedOn(new Date());
            role.setDelete(false);
            this.r_role.save(role);
            return new ModelAndView("redirect:/D_Role/lihat_role");
        }
    }

    @GetMapping(value = "hapus/{id}")
    public ModelAndView hapus(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_Role/hapus");
        M_Role item = this.r_role.findById(id).orElse(null);
        view.addObject("objrole",item);
        return view;
    }

    @PostMapping("is_delete")
    public ModelAndView is_delete(@ModelAttribute M_Role role, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            role.setModifiedBy(new Long(1));
            role.setModifiedOn(new Date());
            role.setDeletedBy(new Long(1));
            role.setDeletedOn(new Date());
            role.setDelete(true);
            this.r_role.save(role);
            return new ModelAndView("redirect:/D_Role/lihat_role");
        }
    }
}
