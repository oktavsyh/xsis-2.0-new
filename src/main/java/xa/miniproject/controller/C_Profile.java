package xa.miniproject.controller;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xa.miniproject.models.M_Biodata;
import xa.miniproject.models.M_Dokumen;
import xa.miniproject.models.M_Keahlian;
import xa.miniproject.models.M_RiwayatKerja;
import xa.miniproject.models.M_RiwayatPendidikan;
import xa.miniproject.repositories.R_Biodata;
import xa.miniproject.repositories.R_Dokumen;
import xa.miniproject.repositories.R_Keahlian;
import xa.miniproject.repositories.R_RiwayatKerja;
import xa.miniproject.repositories.R_RiwayatPendidikan;

/**
 * C_Profile
 */
@Controller
@RequestMapping("profiler")
public class C_Profile {

    private R_Biodata rbiodata;
    private R_RiwayatPendidikan rpendidikan;
    private R_RiwayatKerja rkerja;
    private R_Keahlian rkeahlian;
    private R_Dokumen rdokumen;

    @Autowired

    public C_Profile(R_Dokumen rdokumen,R_Biodata rbiodata, R_RiwayatPendidikan rpendidikan, R_RiwayatKerja rkerja, R_Keahlian rkeahlian) {
        this.rdokumen=rdokumen;
        this.rbiodata = rbiodata;
        this.rpendidikan = rpendidikan;
        this.rkerja = rkerja;
        this.rkeahlian = rkeahlian;
    }
    @GetMapping
    public String cek(){
        return "profile";
    }

    @GetMapping("{id}")
    public ModelAndView profile(@PathVariable("id") Long id) {
        ModelAndView profile = new ModelAndView("D_Profile/profile");
        M_Biodata bio = rbiodata.findById(id).orElse(null);
        M_Dokumen foto = rdokumen.fotoprofil(id);
        System.out.println(foto);
        List<M_RiwayatPendidikan> riwayat = rpendidikan.findByBioId(id);
        List<M_RiwayatKerja> riwayatkerja = rkerja.findByBioId(id);
        List<M_Keahlian> riwayatkeahlian = rkeahlian.findByBioId(id);
        LocalDate birthDate = bio.getDob().toInstant()
      .atZone(ZoneId.systemDefault())
      .toLocalDate();

      Boolean gender = bio.isGender();
      String jk;
      if (gender == true) {
          jk="Laki-Laki";
      } else {
          jk="Perempuan";
      }
       System.out.println(jk);
        int age = Period.between(birthDate, LocalDate.now() ).getYears();
        
        profile.addObject("now",LocalDate.now());
        profile.addObject("pendidikan", riwayat);
        profile.addObject("pekerjaan", riwayatkerja);
        profile.addObject("keahlian", riwayatkeahlian);
        profile.addObject("usia", age);
        profile.addObject("bio", bio);
        profile.addObject("foto", foto);
        profile.addObject("jk", jk);
       
        return profile;
    }
    
}