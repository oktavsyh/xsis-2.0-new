package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_Biodata;
import xa.miniproject.models.M_Catatan;
import xa.miniproject.models.M_Note;
import xa.miniproject.repositories.R_Biodata;
import xa.miniproject.repositories.R_Catatan;
import xa.miniproject.repositories.R_Note;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/D_Catatan/")
public class C_Catatan {
    @Autowired
    private R_Catatan RC;

    @Autowired
    private R_Biodata RB;

    @Autowired R_Note RN;

    @GetMapping(value = "view/{id}")
    public ModelAndView view(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_Catatan/view");
        List<M_Catatan> list = this.RC.findByBioId(id);
        view.addObject("listcatatan", list);
        view.addObject("biodataid", id);
        return view;
    }

    @GetMapping(value = "form")
    public ModelAndView form(){
        ModelAndView view = new ModelAndView("/D_Catatan/form");
        view.addObject("objc",new M_Catatan());
        List <M_Biodata> biodata = this.RB.findAll();
        view.addObject("listb", biodata);
        List <M_Note> note = this.RN.findAll();
        view.addObject("listn", note);
        return view;
    }

    @PostMapping(value = "save")
    public ModelAndView save(@ModelAttribute M_Catatan ctt, BindingResult result){
        if(result.hasErrors()){
            return new ModelAndView("redirect:/D_Catatan/form");
        }else{
            ctt.setCreatedOn(new Date());
            ctt.setCreatedBy(new Long(1));
            ctt.setDelete(false);
            this.RC.save(ctt);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") long id){
        ModelAndView view = new ModelAndView("/D_Catatan/form");
        M_Catatan item = this.RC.findById(id).orElse(null);
        view.addObject("objc",item);
        List<M_Biodata> list = this.RB.findAll();
        view.addObject("listb",list);
        List<M_Note> note = this.RN.findAll();
        view.addObject("listn",note);
        return view;
    }


    @GetMapping(value = "hapus/{id}")
    public ModelAndView hapus(@PathVariable("id") long id){
        ModelAndView view = new ModelAndView("/D_Catatan/hapus");
        M_Catatan item = this.RC.findById(id).orElse(null);
        view.addObject("objc",item);
        return view;
    }

    // Is Delete File
    @PostMapping(value = "delete")
    public ModelAndView softdelete(@ModelAttribute M_Catatan ctt, BindingResult result){
        if (result.hasErrors()) {
            return new ModelAndView("redirect:/delete/{id}");
        } else {

            ctt.setDelete(true);
            ctt.setDeletedBy(new Long(1));
            ctt.setDeletedOn(new Date());
            this.RC.save(ctt);
            return new ModelAndView("redirect:/D_Catatan/view");
        }
    }

}
