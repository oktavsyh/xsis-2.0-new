package xa.miniproject.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xa.miniproject.models.M_Certification_Type;
import xa.miniproject.models.M_Employee;
import xa.miniproject.models.M_Employee_Training;
import xa.miniproject.models.M_Training;
import xa.miniproject.models.M_Training_Organizer;
import xa.miniproject.models.M_Training_Type;
import xa.miniproject.repositories.R_Certification_Type;
import xa.miniproject.repositories.R_Employee;
import xa.miniproject.repositories.R_Employee_Training;
import xa.miniproject.repositories.R_Training;
import xa.miniproject.repositories.R_Training_Organizer;
import xa.miniproject.repositories.R_Training_Type;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employee_training/")
public class C_EmployeeTraining {

    @Autowired
    private R_Employee_Training r_Employee_Training;

    @Autowired
    private R_Employee r_Employee;

    @Autowired
    private R_Training r_Training;

    @Autowired
    private R_Training_Organizer r_Training_Organizer;

    @Autowired
    private R_Training_Type r_Training_Type;

    @Autowired
    private R_Certification_Type r_Certification_Type;

    // Lihat Employee Training
    @GetMapping("view")
    public ModelAndView view() {
        ModelAndView view_employee = new ModelAndView("/D_EmployeeTraining/view");

        List<M_Training> list_training = this.r_Training.findAll();
        view_employee.addObject("list_pelatihan", list_training);

        List<M_Employee_Training> list_employee = this.r_Employee_Training.findemployee1();
        view_employee.addObject("listemployee", list_employee);

        return view_employee;
    }

    // Lihat Employee Training Name AND ID
    @GetMapping("view/{id1}/{id2}")
    public ModelAndView view(@PathVariable("id1") String id, @PathVariable("id2") Long id2) {
        ModelAndView view_employee = new ModelAndView("/D_EmployeeTraining/view");

        List<M_Training> list_training = this.r_Training.findAll();
        view_employee.addObject("list_pelatihan", list_training);

        List<M_Employee_Training> list_employee = this.r_Employee_Training.find_name_training(id, id2);
        view_employee.addObject("listemployee", list_employee);

        return view_employee;
    }

    // Form Tambah Pelatihan Karyawan
    @GetMapping("form")
    public ModelAndView form() {
        ModelAndView view = new ModelAndView("/D_EmployeeTraining/form_tambah");
        view.addObject("obj_employee_training", new M_Employee_Training());

        List<M_Employee> list_employee = this.r_Employee.findAll();
        view.addObject("list_karyawan", list_employee);

        List<M_Training> list_training = this.r_Training.findAll();
        view.addObject("list_pelatihan", list_training);

        List<M_Training_Organizer> list_training_organizer = this.r_Training_Organizer.findAll();
        view.addObject("list_penyelenggara", list_training_organizer);

        List<M_Training_Type> list_training_type = this.r_Training_Type.findAll();
        view.addObject("list_jenis_pelatihan", list_training_type);

        List<M_Certification_Type> list_certification_type = this.r_Certification_Type.findAll();
        view.addObject("list_jenis_sertifikasi", list_certification_type);

        return view;
    }

    // Controller Untuk Save Setelah Form Tambah Pelatihan Karyawan
    @PostMapping("save")
    public ModelAndView save_employee_training(@ModelAttribute M_Employee_Training m_Employee_Training,
            BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            m_Employee_Training.setCreatedBy(new Long(1));
            m_Employee_Training.setCreatedOn(new Date());
            m_Employee_Training.setIsDelete(false);
            m_Employee_Training.setStatus("submit");
            this.r_Employee_Training.save(m_Employee_Training);
            return new ModelAndView("redirect:/employee_training/view");
        }
    }

    // Form Edit #Tahap 1
    @GetMapping("edit/{id}")
    public ModelAndView edit_employee_training(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_EmployeeTraining/form_edit");
        M_Employee_Training item = this.r_Employee_Training.findById(id).orElse(null);
        view.addObject("obj_employee_training", item);

        List<M_Employee> list_employee = this.r_Employee.findAll();
        view.addObject("list_karyawan", list_employee);

        List<M_Training> list_training = this.r_Training.findAll();
        view.addObject("list_pelatihan", list_training);

        List<M_Training_Organizer> list_training_organizer = this.r_Training_Organizer.findAll();
        view.addObject("list_penyelenggara", list_training_organizer);

        List<M_Training_Type> list_training_type = this.r_Training_Type.findAll();
        view.addObject("list_jenis_pelatihan", list_training_type);

        List<M_Certification_Type> list_certification_type = this.r_Certification_Type.findAll();
        view.addObject("list_jenis_sertifikasi", list_certification_type);

        return view;
    }

    // Controller Untuk Save Setelah Form Edit Pelatihan Karyawan #Tahap 2
    @PostMapping("save_edit")
    public ModelAndView save_edit(@ModelAttribute M_Employee_Training m_Employee_Training, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            m_Employee_Training.setCreatedBy(new Long(10));
            m_Employee_Training.setModifiedBy(new Long(10));
            m_Employee_Training.setModifiedOn(new Date());
            m_Employee_Training.setIsDelete(false);
            m_Employee_Training.setStatus("submit");
            this.r_Employee_Training.save(m_Employee_Training);
            return new ModelAndView("redirect:/employee_training/view");
        }
    }

    // is_delete [Passing ke Form Hapus Pelatihan Karyawan] #Tahap 1
    @GetMapping("hapus/{id}")
    public ModelAndView hapus_employee_training(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_EmployeeTraining/form_hapus");
        M_Employee_Training item = this.r_Employee_Training.findById(id).orElse(null);
        view.addObject("obj_employee_training", item);

        List<M_Employee> list_employee = this.r_Employee.findAll();
        view.addObject("list_karyawan", list_employee);

        List<M_Training> list_training = this.r_Training.findAll();
        view.addObject("list_pelatihan", list_training);

        List<M_Training_Organizer> list_training_organizer = this.r_Training_Organizer.findAll();
        view.addObject("list_penyelenggara", list_training_organizer);

        List<M_Training_Type> list_training_type = this.r_Training_Type.findAll();
        view.addObject("list_jenis_pelatihan", list_training_type);

        List<M_Certification_Type> list_certification_type = this.r_Certification_Type.findAll();
        view.addObject("list_jenis_sertifikasi", list_certification_type);

        return view;
    }

    // is_delete #Tahap 2 Untuk Hapus tanpa menghilangkan row DB
    @PostMapping("is_delete")
    public ModelAndView is_delete(@ModelAttribute M_Employee_Training m_Employee_Training, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            m_Employee_Training.setDeletedOn(new Date());
            m_Employee_Training.setDeletedBy(new Long(10));
            m_Employee_Training.setIsDelete(true);
            m_Employee_Training.setStatus("Delete");
            this.r_Employee_Training.save(m_Employee_Training);
            return new ModelAndView("redirect:/employee_training/view");
        }
    }

    // is_persetujuan #Tahap 1
    @GetMapping("persetujuan/{id}")
    public ModelAndView persetujuan(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_EmployeeTraining/form_persetujuan");
        M_Employee_Training item = this.r_Employee_Training.findById(id).orElse(null);
        view.addObject("obj_employee_training", item);

        List<M_Employee> list_employee = this.r_Employee.findAll();
        view.addObject("list_karyawan", list_employee);

        List<M_Training> list_training = this.r_Training.findAll();
        view.addObject("list_pelatihan", list_training);

        List<M_Training_Organizer> list_training_organizer = this.r_Training_Organizer.findAll();
        view.addObject("list_penyelenggara", list_training_organizer);

        List<M_Training_Type> list_training_type = this.r_Training_Type.findAll();
        view.addObject("list_jenis_pelatihan", list_training_type);

        List<M_Certification_Type> list_certification_type = this.r_Certification_Type.findAll();
        view.addObject("list_jenis_sertifikasi", list_certification_type);

        return view;
    }

    // is_persetujuan #Tahap 2
    @PostMapping("is_persetujuan")
    public ModelAndView is_persetujuan(@ModelAttribute M_Employee_Training m_Employee_Training, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            m_Employee_Training.setModifiedBy(new Long(10));
            m_Employee_Training.setIsDelete(false);
            m_Employee_Training.setStatus("Approved");
            this.r_Employee_Training.save(m_Employee_Training);
            return new ModelAndView("redirect:/employee_training/view");
        }
    }

    // is_selesai #Tahap 1
    @GetMapping("selesai/{id}")
    public ModelAndView selesai(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_EmployeeTraining/form_selesai");
        M_Employee_Training item = this.r_Employee_Training.findById(id).orElse(null);
        view.addObject("obj_employee_training", item);

        List<M_Employee> list_employee = this.r_Employee.findAll();
        view.addObject("list_karyawan", list_employee);

        List<M_Training> list_training = this.r_Training.findAll();
        view.addObject("list_pelatihan", list_training);

        List<M_Training_Organizer> list_training_organizer = this.r_Training_Organizer.findAll();
        view.addObject("list_penyelenggara", list_training_organizer);

        List<M_Training_Type> list_training_type = this.r_Training_Type.findAll();
        view.addObject("list_jenis_pelatihan", list_training_type);

        List<M_Certification_Type> list_certification_type = this.r_Certification_Type.findAll();
        view.addObject("list_jenis_sertifikasi", list_certification_type);

        return view;
    }

    // is_selesai #Tahap 2
    @PostMapping("is_selesai")
    public ModelAndView is_selesai(@ModelAttribute M_Employee_Training m_Employee_Training, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            m_Employee_Training.setModifiedOn(new Date());
            m_Employee_Training.setIsDelete(false);
            m_Employee_Training.setStatus("Done");
            this.r_Employee_Training.save(m_Employee_Training);
            return new ModelAndView("redirect:/employee_training/view");
        }
    }

    // Form List Training
    @GetMapping("list_training/{id}")
    public ModelAndView list_training(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_EmployeeTraining/list_training");

        List<M_Training> list_training = this.r_Training.cari_training(id);
        view.addObject("list_training", list_training);

        return view;
    }

    @GetMapping(value = "/getbioRP/{id}")
    public List<M_Employee_Training> list(@PathVariable("id") Long id) {
        return this.r_Employee_Training.findemployee2(id);
    }

    @GetMapping("/getET/{id}")
    public M_Employee_Training getET(@PathVariable("id") Long id) {
        return this.r_Employee_Training.findById(id).orElse(null);
    }

    // Validate Employee Id
    @GetMapping(value = "/validate/{employee_id}")
    public M_Employee_Training validate(@PathVariable("employee_id") Long v2) {
        return this.r_Employee_Training.validateFullName(v2);
    }

}