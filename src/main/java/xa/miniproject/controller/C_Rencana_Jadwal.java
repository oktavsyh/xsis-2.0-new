package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.*;
import xa.miniproject.repositories.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/D_Rencana_Jadwal/")
public class C_Rencana_Jadwal {
    @Autowired
    private R_Rencana_Jadwal renrepo;

    @Autowired
    private R_Rencana_Jadwal_Detail rjdrepo;

    @Autowired
    private R_RO rorepo;

    @Autowired
    private R_TRO trorepo;

    @Autowired
    private R_Schedule_Type schrepo;

    @Autowired
    private R_Biodata biorepo;

    @GetMapping(value = "view")
    public ModelAndView view() {
        ModelAndView view = new ModelAndView("D_Rencana_Jadwal/view");
        List<M_Rencana_Jadwal> listren = this.renrepo.findAll();
        view.addObject("listren", listren);
        return view;
    }

    @GetMapping(value = "view_by_date/{date1}/{date2}")
    public ModelAndView view_by_date(@PathVariable("date1") Date date1, @PathVariable("date2") Date date2) {
        ModelAndView view = new ModelAndView("D_Rencana_Jadwal/view_by_date");

        List<M_Rencana_Jadwal> listren = this.renrepo.findByDate(date1, date2);
        view.addObject("listren", listren);

        List<M_RO> rolist = this.rorepo.findAll();
        view.addObject("rolist", rolist);

        List<M_TRO> trolist = this.trorepo.findAll();
        view.addObject("trolist", trolist);

        List<M_Schedule_Type> schlist = this.schrepo.findAll();
        view.addObject("schlist", schlist);

        return view;
    }

    @GetMapping(value = "form_tambah_rencana")
    public ModelAndView form_tambah_rencana() {
        ModelAndView view = new ModelAndView("D_Rencana_Jadwal/form_tambah_rencana");
        view.addObject("objrencana", new M_Rencana_Jadwal());

        List<M_Rencana_Jadwal_Detail> listrendet = this.rjdrepo.findAll();
        view.addObject("listrendet", listrendet);

        List<M_RO> rolist = this.rorepo.findAll();
        view.addObject("rolist", rolist);

        List<M_TRO> trolist = this.trorepo.findAll();
        view.addObject("trolist", trolist);

        List<M_Schedule_Type> schlist = this.schrepo.findAll();
        view.addObject("schlist", schlist);

        List<M_Biodata> listbiodata = this.biorepo.findAll();
        view.addObject("listbiodata", listbiodata);

        return view;
    }

    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_Rencana_Jadwal/edit_rencana");
        M_Rencana_Jadwal rencana = this.renrepo.findById(id)
                .orElseThrow(() ->
                        new IllegalArgumentException("Invalid Rencana ID :" + id));

        view.addObject("rencana", rencana);

        List<M_Rencana_Jadwal_Detail> listrendet = this.rjdrepo.findAll();
        view.addObject("listrendet", listrendet);

        List<M_RO> rolist = this.rorepo.findAll();
        view.addObject("rolist", rolist);

        List<M_TRO> trolist = this.trorepo.findAll();
        view.addObject("trolist", trolist);

        List<M_Schedule_Type> schlist = this.schrepo.findAll();
        view.addObject("schlist", schlist);

        List<M_Biodata> listbiodata = this.biorepo.findAll();
        view.addObject("listbiodata", listbiodata);

        return view;
    }

    @GetMapping(value = "edit_rencana")
    public ModelAndView edit_rencana() {
        ModelAndView view = new ModelAndView("D_Rencana_Jadwal/edit_rencana");
        view.addObject("rencana", new M_Rencana_Jadwal());
        List<M_Rencana_Jadwal_Detail> listrendet = this.rjdrepo.findAll();
        view.addObject("listrendet", listrendet);

        List<M_RO> rolist = this.rorepo.findAll();
        view.addObject("rolist", rolist);

        List<M_TRO> trolist = this.trorepo.findAll();
        view.addObject("trolist", trolist);

        List<M_Schedule_Type> schlist = this.schrepo.findAll();
        view.addObject("schlist", schlist);

        List<M_Biodata> listbiodata = this.biorepo.findAll();
        view.addObject("listbiodata", listbiodata);

        return view;
    }

    @PostMapping(value = "save")
    public ModelAndView save(@ModelAttribute M_Rencana_Jadwal rencana, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("redirect:/D_Rencana_Jadwal/form_tambah_rencana");
        } else {
            rencana.setCreatedOn(new Date());
            rencana.setCreatedBy(new Long(1));
            rencana.setDelete(false);
            rencana.setModifiedOn(new Date());
            rencana.setModifiedBy(new Long(1));
            this.renrepo.save(rencana);
            return new ModelAndView("redirect:/D_Rencana_Jadwal/view");
        }
    }

    @GetMapping(value = "delete_update/{id}")
    public ModelAndView delete_update(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_Rencana_Jadwal/delete_update");
        M_Rencana_Jadwal item = this.renrepo.findById(id).orElse(null);
        view.addObject("rencana", item);
        return view;
    }

    @GetMapping(value = "/getRencana/{id}")
    public M_Rencana_Jadwal getRencana(@PathVariable("id") Long id) {
        return this.renrepo.findById(id).orElse(null);
    }

    @PostMapping(value = "/saveRencana/")
    public M_Rencana_Jadwal saveRencana(@RequestBody M_Rencana_Jadwal item) {
        return this.renrepo.save(item);
    }

    @PutMapping(value = "/editRencana/{id}")
    public M_Rencana_Jadwal editRencana(@RequestBody M_Rencana_Jadwal item) {
        return this.renrepo.save(item);
    }

    @GetMapping(value = "/idmax")
    public Long idmax() {
        return this.renrepo.findBymaxid();
    }

    @PostMapping(value = "/saveRJD")
    public M_Rencana_Jadwal_Detail save(@RequestBody M_Rencana_Jadwal_Detail item){return this.rjdrepo.save(item);}

    @GetMapping(value = "view_list_biodata")
    public ModelAndView view_list_biodata(){
        ModelAndView view = new ModelAndView("D_Rencana_Jadwal/view_list_biodata");

        List<M_Biodata> listbio = this.biorepo.findAll();
        view.addObject("listbiodata", listbio);
        return view;
    }
}
