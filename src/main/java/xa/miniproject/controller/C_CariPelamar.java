package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_ViewPelamar;
import xa.miniproject.repositories.R_Biodata;
import xa.miniproject.repositories.R_ViewPelamar;

import java.util.List;

@RestController
@RequestMapping(value = "/D_CariPelamar/")
public class C_CariPelamar {
    @Autowired
    private R_Biodata repoBio;
    @Autowired
    private R_ViewPelamar repoRP;

    @GetMapping(value = "viewlain/{string}")
    public ModelAndView viewlain(@PathVariable("string") String item){
        ModelAndView RP = new ModelAndView("/D_CariPelamar/viewlain");
        List<M_ViewPelamar> listbio = this.repoRP.findpelamarname(item);
        RP.addObject("listVP", listbio);
        System.out.println(listbio);
        return RP;
    }

    @GetMapping(value = "view")
    public ModelAndView view(){
        ModelAndView RP = new ModelAndView("/D_CariPelamar/view");
        List<M_ViewPelamar> listbio = this.repoRP.findpelamar();
        RP.addObject("listVP", listbio);
        System.out.println(listbio);
        return RP;
    }

    @GetMapping(value = "/viewCP")
    public List<M_ViewPelamar> viewCP(){ return this.repoRP.findpelamar();}

    @GetMapping(value = "full")
    public ModelAndView full(){
        ModelAndView RP = new ModelAndView("/D_CariPelamar/full");
        return RP;
    }
}