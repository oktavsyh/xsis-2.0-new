package xa.miniproject.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xa.miniproject.Services.SendEmailService;
import xa.miniproject.models.M_Biodata;
import xa.miniproject.repositories.R_Biodata;

/**
 * C_SendEmail
 */
@RestController
@RequestMapping("send")
public class C_SendEmail {
    private Logger log = LoggerFactory.getLogger(C_SendEmail.class);
    private SendEmailService service;

    private R_Biodata r_Biodata;

    @Autowired
    public C_SendEmail(SendEmailService service, R_Biodata r_Biodata) {
        this.service = service;
        this.r_Biodata = r_Biodata;
    }

    @GetMapping("{id}")
    public String sendsucces(@PathVariable("id") Long id){
        M_Biodata biodata = r_Biodata.findById(new Long(1)).orElse(null);
        try {
//            service.sendNotificaitoin(biodata);
        } catch (Exception e) {
  
            log.info("HERE ERROR SENDING MASSAGE "+e.getMessage());
        }      
        return "Succes";
    }
}