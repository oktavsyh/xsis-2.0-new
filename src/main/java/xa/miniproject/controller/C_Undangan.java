package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_Biodata;
import xa.miniproject.models.M_Undangan;
import xa.miniproject.models.M_UndanganDetail;
import xa.miniproject.models.M_ViewPelamar;
import xa.miniproject.repositories.*;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/D_Penjadwalan_Undangan/")
public class C_Undangan {
    @Autowired
    private R_Biodata RB;

    @Autowired
    private R_Undangan RU;

    @Autowired
    private R_UndanganDetail RUD;

    @Autowired
    private R_Schedule_Type RS;

    @Autowired
    private R_RO RRO;

    @Autowired
    private R_TRO RTRO;

    @Autowired
    private R_ViewPelamar RVP;

    @GetMapping(value = "view")
    public ModelAndView view() {
        ModelAndView view = new ModelAndView("/D_Penjadwalan_Undangan/view");
        List<M_UndanganDetail> listu = this.RUD.finduddId();
        view.addObject("listu", listu);
        return view;
    }

    @GetMapping(value = "form")
    public ModelAndView form(){
        ModelAndView view = new ModelAndView("/D_Penjadwalan_Undangan/form");
        view.addObject("objud",new M_UndanganDetail());
        List <M_Biodata> biodata = this.RB.findAll();
        view.addObject("listb", biodata);
        List <M_Undangan> undangan = this.RU.findAll();
        view.addObject("listu", undangan);
        return view;
    }

    @PostMapping(value = "save")
    public ModelAndView save(@ModelAttribute M_Undangan udg, BindingResult result){
        if(result.hasErrors()){
            return new ModelAndView("redirect:/D_Penjadwalan_Undangan/form");
        }else{
            udg.setCreatedOn(new Date());
            udg.setCreatedBy(new Long(1));
            udg.setDelete(false);
            this.RU.save(udg);
            return new ModelAndView("redirect:/D_Penjadwalan_Undangan/view");
        }
    }

    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") long id){
        ModelAndView view = new ModelAndView("/D_Penjadwalan_Undangan/form");
        M_UndanganDetail item = this.RUD.findById(id).orElse(null);
        view.addObject("objud",item);
        List<M_Biodata> listb = this.RB.findAll();
        view.addObject("listb",listb);
        List<M_Undangan> listu = this.RU.findAll();
        view.addObject("listu",listu);
        return view;
    }

    @GetMapping(value = "detail/{id}")
    public ModelAndView detail(@PathVariable("id") long id) {
        ModelAndView view = new ModelAndView("/D_Penjadwalan_Undangan/detail");
        M_UndanganDetail listud = this.RUD.findById(id).orElse(null);
        view.addObject("listud", listud);
        return view;
    }

    @GetMapping(value = "hapus/{id}")
    public ModelAndView hapus(@PathVariable("id") long id){
        ModelAndView view = new ModelAndView("/D_Penjadwalan_Undangan/hapus");
        M_UndanganDetail item = this.RUD.findById(id).orElse(null);
        view.addObject("objud",item);
        return view;
    }

    @PostMapping(value = "delete")
    public ModelAndView softdelete(@ModelAttribute M_Undangan udg, BindingResult result){
        if (result.hasErrors()) {
            return new ModelAndView("redirect:/delete/{id}");
        } else {

            udg.setDelete(true);
            udg.setDeletedBy(new Long(1));
            udg.setDeletedOn(new Date());
            this.RU.save(udg);
            return new ModelAndView("redirect:/D_Penjadwalan_Undangan/view");
        }
    }

}