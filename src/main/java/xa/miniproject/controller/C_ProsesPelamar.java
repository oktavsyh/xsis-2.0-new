package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.*;
import xa.miniproject.repositories.*;

import java.util.List;

@RestController
@RequestMapping(value = "/D_ProsesPelamar/")
public class C_ProsesPelamar {
    @Autowired
    private R_ViewProsesPelamar repoPP;
    @Autowired
    private R_Biodata repoBio;
    @Autowired
    private R_Rencana_Jadwal_Detail repoRJD;
    @Autowired
    private R_UndanganDetail repoUD;

    @GetMapping(value = "view")
    public ModelAndView view() {
        ModelAndView PP = new ModelAndView("/D_ProsesPelamar/view");
        List<M_ViewProsesPelamar> listprosesfalse = this.repoPP.findprosesfalse();
        PP.addObject("listprosesfalse", listprosesfalse);
        List<M_ViewProsesPelamar> listprosestrue = this.repoPP.findprosestrue();
        PP.addObject("listprosestrue", listprosestrue);
        return PP;
    }

    @GetMapping(value = "/detailproses/{id}")
    public ModelAndView detailproses(@PathVariable("id") Long id) {
        ModelAndView DP = new ModelAndView("/D_ProsesPelamar/detailproses");
        List<M_Rencana_Jadwal_Detail> listRenJad = this.repoRJD.findJadByBio(id);
        DP.addObject("listRenJad", listRenJad);
        List<M_UndanganDetail> listUnJad = this.repoUD.findUndByBio(id);
        DP.addObject("listUnJad", listUnJad);
        return DP;
    }

    @GetMapping(value = "/getPP/{id}")
    public M_Biodata getPP(@PathVariable("id") Long id) {
        return this.repoBio.findById(id).orElse(null);
    }

    @PutMapping(value = "/editPP/{id}")
    public M_Biodata updatePP(@RequestBody M_Biodata item) {
        return this.repoBio.save(item);
    }

    @GetMapping(value = "/getJD/{id}")
    public M_Rencana_Jadwal_Detail getJD(@PathVariable("id") Long id) {
        return this.repoRJD.findById(id).orElse(null);
    }

    @PutMapping(value = "/editJD/{id}")
    public M_Rencana_Jadwal_Detail updateJD(@RequestBody M_Rencana_Jadwal_Detail item) {
        return this.repoRJD.save(item);
    }

    @GetMapping(value = "/getUD/{id}")
    public M_UndanganDetail getUD(@PathVariable("id") Long id) {
        return this.repoUD.findById(id).orElse(null);
    }

    @PutMapping(value = "/editUD/{id}")
    public M_UndanganDetail updateUD(@RequestBody M_UndanganDetail item) {
        return this.repoUD.save(item);
    }

}
