package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xa.miniproject.models.M_Role;
import xa.miniproject.repositories.R_Role;

import java.util.List;

@RestController
@RequestMapping(value="/api/role")
public class API_Role {
    @Autowired
    private R_Role role;

    @GetMapping(value="/{code}")
    public Long validate(@PathVariable("code") String p1)
    {
        return this.role.validate(p1);
    }
}
