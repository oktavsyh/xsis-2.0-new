package xa.miniproject.controller;

        // import com.sun.deploy.security.SelectableSecurityManager;
        import org.hibernate.NonUniqueResultException;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.*;
        import xa.miniproject.models.M_Address;
        import xa.miniproject.models.M_Biodata;
        import xa.miniproject.models.M_RiwayatPendidikan;
        import xa.miniproject.repositories.R_Address;
        import xa.miniproject.repositories.R_Biodata;
        import xa.miniproject.repositories.R_RiwayatPendidikan;

        import java.util.List;

@RestController
@RequestMapping(value="/api/recruitment")
@CrossOrigin(origins = "*")
public class API {

    @Autowired
    private R_Address addrepo;
    @Autowired
    private R_Biodata biorepo;
    @Autowired
    private R_RiwayatPendidikan repoRP;

    @GetMapping(value="/")
    public List<M_Address> list() {
        return this.addrepo.findAll();
    }

    @GetMapping(value = "/viewRP")
    public List<M_RiwayatPendidikan> list2(){
        return this.repoRP.findAll();
    }

    @GetMapping(value="/{id}")
    public M_Address listById(@PathVariable("id") Long id) {
        return this.addrepo.findById(id).orElse(null);
    }

    @PostMapping(value="/" , consumes = "application/json")
    public M_Address save(@RequestBody M_Address add){
        return this.addrepo.save(add);}

    @GetMapping(value = "/apimax")
    public Long maxid(){
        return this.biorepo.findBymaxid();
    }

    @PostMapping(value="/saveRP/{biodataId}" , consumes = "application/json")
    public M_RiwayatPendidikan save(@PathVariable("biodataId") Long bioid,@RequestBody M_RiwayatPendidikan add){
        add.setBiodataid(new Long(bioid));
        return this.repoRP.save(add);
    }

    @GetMapping(value = "/viewBio")
    public List<M_Biodata> listbio(){
        return this.biorepo.findAll();
    }

    @GetMapping(value = "/viewRP/{id}")
    public List<M_RiwayatPendidikan> listByBioId(@PathVariable("id") Long id) {
        return this.repoRP.findByBioId(id);
    }


}