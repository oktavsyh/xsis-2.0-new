package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_Family_Relation;
import xa.miniproject.repositories.R_FamilyRelation;
import xa.miniproject.repositories.R_FamilyTreeType;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/D_FamilyType/")
public class C_FamilyType {
    @Autowired
    private R_FamilyRelation r_familyRelation;

    @Autowired
    private R_FamilyTreeType r_familyTreeType;

    @GetMapping(value = "view")
    public ModelAndView view(){
        ModelAndView view = new ModelAndView("/D_FamilyType/view");
        List<M_Family_Relation> list= this.r_familyRelation.findAll();
        view.addObject("listrelation", list);
        return view;
    }

    @GetMapping(value = "form")
    public ModelAndView form(){
        ModelAndView form = new ModelAndView("/D_FamilyType/form");
        form.addObject("objrole",new M_Family_Relation());
        return form;
    }

    @PostMapping(value = "save")
    public ModelAndView save(@ModelAttribute M_Family_Relation relation, BindingResult result){
        if(result.hasErrors()){
            return new ModelAndView("redirect:/D_FamilyType/form");
        }
        else{
            relation.setCreateBy(new Long(1));
            relation.setCreateOn(new Date());
            relation.setIsDelete(false);
            this.r_familyRelation.save(relation);
            return new ModelAndView("redirect:/D_FamilyType/view");
        }
    }

    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_FamilyType/form_edit");
        M_Family_Relation item = this.r_familyRelation.findById(id).orElse(null);
        view.addObject("objrole",item);
        return view;
    }

    @PostMapping(value = "save_edit")
    public ModelAndView save_edit(@ModelAttribute M_Family_Relation relation , BindingResult result){
        if(result.hasErrors()){
            return new ModelAndView("redirect:/D_FamilyType/form_edit");
        }
        else{
            relation.setModifiedBy(new Long(1));
            relation.setModifiedOn(new Date());
            relation.setIsDelete(false);
            this.r_familyRelation.save(relation);
            return new ModelAndView("redirect:/D_FamilyType/view");
        }
    }


}
