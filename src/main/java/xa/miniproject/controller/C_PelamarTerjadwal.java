package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.*;
import xa.miniproject.repositories.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value="/D_PelamarTerjadwal/")
public class C_PelamarTerjadwal {
    @Autowired
    private R_ViewPelamar repoPT;
    @Autowired
    private R_Rencana_Jadwal_Detail repoRJD;
    @Autowired
    private R_Rencana_Jadwal repoRJ;
    @Autowired
    private R_RO repoRO;
    @Autowired
    private R_TRO repoTRO;
    @Autowired
    private R_Schedule_Type repoST;

    @GetMapping(value = "view/{id}")
    public ModelAndView lihat(@PathVariable("id") Long id){
        ModelAndView lihat = new ModelAndView("D_PelamarTerjadwal/terjadwal");
        List<M_Rencana_Jadwal_Detail> listjadwal = this.repoRJD.findbyrencid(id);
        lihat.addObject("listjadwal", listjadwal);
        return lihat;
    }

    @GetMapping(value = "undang/{id}")
    public ModelAndView undang(@PathVariable("id") Long id){
        ModelAndView lihat = new ModelAndView("D_PelamarTerjadwal/undang");
        M_Rencana_Jadwal_Detail object = this.repoRJD.findById(id).orElse(null);
        lihat.addObject("objundang", object);
        return lihat;
    }

    @GetMapping(value = "ubahjadwal")
    public ModelAndView form() {
        ModelAndView form = new ModelAndView("/D_PelamarTerjadwal/ubahjadwal");
        form.addObject("objRJ", new M_Rencana_Jadwal());
        List<M_RO> listro = this.repoRO.findAll();
        form.addObject("listro",listro);
        List<M_TRO> listtro = this.repoTRO.findAll();
        form.addObject("listtro",listtro);
        List<M_Schedule_Type> listST = this.repoST.findAll();
        form.addObject("listST",listST);
        return form;
    }

    @GetMapping(value = "ubahjadwal/{id}")
    public ModelAndView form(@PathVariable("id") Long id) {
        ModelAndView form = new ModelAndView("/D_PelamarTerjadwal/ubahjadwal");
        M_Rencana_Jadwal_Detail item = this.repoRJD.findById(id).orElse(null);
        form.addObject("objRJ", item);
        List<M_RO> listro = this.repoRO.findAll();
        form.addObject("listro",listro);
        List<M_TRO> listtro = this.repoTRO.findAll();
        form.addObject("listtro",listtro);
        List<M_Schedule_Type> listST = this.repoST.findAll();
        form.addObject("listST",listST);
        return form;
    }

    @GetMapping(value = "/maxid/")
    public Long maxid(){
        return this.repoRJ.findBymaxid();
    }

    @GetMapping(value = "/detailemail/{id}")
    public M_Rencana_Jadwal_Detail detailemail(@PathVariable("id") Long id){return this.repoRJD.findById(id).orElse(null);}

    @PostMapping(value = "/saveRJ")
    public M_Rencana_Jadwal save(@RequestBody M_Rencana_Jadwal item){return this.repoRJ.save(item);}

    @GetMapping(value = "/getRJ/")
    public List<M_Rencana_Jadwal> getRJ(){return this.repoRJ.findAll();}

    @GetMapping(value = "/getRJ/{id}")
    public M_Rencana_Jadwal getRJid(@PathVariable("id") Long id){return this.repoRJ.findById(id).orElse(null);}

    @PutMapping(value = "/editRJ/{id}")
    public M_Rencana_Jadwal editRJ(@RequestBody M_Rencana_Jadwal item){return this.repoRJ.save(item);}

    @GetMapping(value = "/getRJD/{id}")
    public M_Rencana_Jadwal_Detail getRJD(@PathVariable("id") Long id){return this.repoRJD.findById(id).orElse(null);}

    @PutMapping(value = "/editRJD/{id}")
    public M_Rencana_Jadwal_Detail editRJD(@RequestBody M_Rencana_Jadwal_Detail item){return this.repoRJD.save(item);}

    @GetMapping(value = "/countdata/{id}")
    public Long countdata(@PathVariable("id") Long id){ return this.repoRJD.findjumlahdata(id);}

    @PostMapping(value = "/saveRJD")
    public M_Rencana_Jadwal_Detail save(@RequestBody M_Rencana_Jadwal_Detail item){return this.repoRJD.save(item);}
}
