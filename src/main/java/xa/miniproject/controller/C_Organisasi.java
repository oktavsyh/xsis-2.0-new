package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_Biodata;
import xa.miniproject.models.M_Organisasi;
import xa.miniproject.repositories.R_Biodata;
import xa.miniproject.repositories.R_Organisasi;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/D_Organisasi/")
public class C_Organisasi {
    @Autowired
    private R_Organisasi RO;

    @Autowired
    private R_Biodata RB;

    @GetMapping(value = "view/{id}")
    public ModelAndView view(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_Organisasi/view");
        List<M_Organisasi> list = this.RO.findByBioId(id);
        view.addObject("listorganisasi", list);
        view.addObject("biodataid", id);
        return view;
    }

//    @PathVariable ("id") Long id
    @GetMapping(value = "form")
    public ModelAndView form(){
        ModelAndView view = new ModelAndView("/D_Organisasi/form");
        view.addObject("objo",new M_Organisasi());
        List <M_Biodata> biodata = this.RB.findAll();
        view.addObject("listb", biodata);
        return view;
    }

    @PostMapping(value = "save")
    public ModelAndView save(@ModelAttribute M_Organisasi org, BindingResult result){
        if(result.hasErrors()){
            return new ModelAndView("redirect:/D_Organisasi/form");
        }else{
            org.setCreatedOn(new Date());
            org.setCreatedBy(new Long(1));
            org.setDelete(false);
            this.RO.save(org);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") long id){
        ModelAndView view = new ModelAndView("/D_Organisasi/form");
        M_Organisasi item = this.RO.findById(id).orElse(null);
        view.addObject("objo",item);
        List<M_Biodata> list = this.RB.findAll();
        view.addObject("listb",list);
        return view;
    }


    @GetMapping(value = "hapus/{id}")
    public ModelAndView hapus(@PathVariable("id") long id){
        ModelAndView view = new ModelAndView("/D_Organisasi/hapus");
        M_Organisasi item = this.RO.findById(id).orElse(null);
        view.addObject("objo",item);
        return view;
    }

    // Is Delete File
    @PostMapping(value = "delete")
    public ModelAndView softdelete(@ModelAttribute M_Organisasi org, BindingResult result){
        if (result.hasErrors()) {
            return new ModelAndView("redirect:/delete/{id}");
        } else {

            org.setDelete(true);
            org.setDeletedBy(new Long(1));
            org.setDeletedOn(new Date());
            this.RO.save(org);
            return new ModelAndView("redirect:/D_Organisasi/view");
        }
    }

}
