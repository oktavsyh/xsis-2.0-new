package xa.miniproject.controller;

import jdk.nashorn.internal.objects.annotations.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_Addrbook;
import xa.miniproject.models.M_Biodata;
import xa.miniproject.models.M_Role;
import xa.miniproject.models.M_Userrole;
import xa.miniproject.repositories.R_Addrbook;
import xa.miniproject.repositories.R_Biodata;
import xa.miniproject.repositories.R_Role;
import xa.miniproject.repositories.R_Userrole;

import java.util.List;

@RestController
@RequestMapping(value = "/D_Pengguna/")
public class C_Pengguna {
    @Autowired
    private R_Biodata repoBio;
    @Autowired
    private R_Role repoRole;
    @Autowired
    private R_Addrbook repoAddr;
    @Autowired
    private R_Userrole repoUR;

    @GetMapping(value = "blablabla")
    public ModelAndView blablabla(){
        ModelAndView blabla = new ModelAndView("/D_Pengguna/blablabla");
        return blabla;
    }

    @GetMapping(value = "view")
    public ModelAndView view(){
        ModelAndView view = new ModelAndView("/D_Pengguna/view");
        List<M_Role> item = this.repoRole.findAll();
        view.addObject("objectrole", item);
        return view;
    }

    @GetMapping(value = "getfullname")
    public List<M_Biodata> getfullname(){return this.repoBio.findAll();}

    @GetMapping(value = "getinfo/{string}")
    public M_Biodata getinfo(@PathVariable("string") String a){return this.repoBio.findByFullName(a);}

    @GetMapping(value = "getrole")
    public ModelAndView viewlain(){
        ModelAndView view = new ModelAndView("/D_Pengguna/view");
        return view;
    }

    @GetMapping(value = "validateemail/{string}")
    public Long validate(@PathVariable("string") String v1){return this.repoAddr.validateemail(v1);}

    @GetMapping(value = "getdataAB/{email}")
    public M_Addrbook getdataAB(@PathVariable("email") String hei){return this.repoAddr.getdataAB(hei);}

    @GetMapping(value = "getmaxidAB")
    public Long getmaxid(){return this.repoAddr.findmaxid();}

    @GetMapping(value = "getAddrbook")
    public List<M_Addrbook> getAddrbook(){return this.repoAddr.findAllfalse();}

    @GetMapping(value = "getAddrbookbyid/{id}")
    public M_Addrbook findfalsebyid(@PathVariable("id") Long ID){return this.repoAddr.findfalsebyid(ID);}

    @PostMapping(value = "saveAddrbook")
    public M_Addrbook saveAddrbook(@RequestBody M_Addrbook item){return this.repoAddr.save(item);}

    @PutMapping(value = "editAddrbook/{id}")
    public M_Addrbook editAddrbook(@RequestBody M_Addrbook item){return this.repoAddr.save(item);}

    @GetMapping(value = "getdataUR/{id}")
    public List<M_Userrole> getdataUR(@PathVariable("id") Long id){return this.repoUR.finddataUR(id);}

    @PutMapping(value = "editUR/{id}")
    public M_Userrole editUR(@RequestBody M_Userrole item){return this.repoUR.save(item);}

    @PostMapping(value = "saveUR")
    public M_Userrole saveUR(@RequestBody M_Userrole item){return this.repoUR.save(item);}

    @GetMapping(value = "getdataBio/{fullname}")
    public M_Biodata getdataBio(@PathVariable("fullname") String s){return this.repoBio.getinfo(s);}

    @PutMapping(value = "editBio/{id}")
    public M_Biodata editBio(@RequestBody M_Biodata item){return this.repoBio.save(item);}

}