package xa.miniproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class Home {
    @GetMapping("index")
   public String index(){
       return "index";
   }

//     @GetMapping(value = "index")
//     public ModelAndView index(HttpSession session) {
//         ModelAndView view = new ModelAndView("/index");
//         return view;
//
//     }
     @GetMapping(value = "indexadmin")
     public ModelAndView indexadmin(HttpSession session) {
         ModelAndView view = new ModelAndView("/indexadmin");
         return view;

     }
}