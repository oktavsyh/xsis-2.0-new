package xa.miniproject.controller;

import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import xa.miniproject.models.M_Biodata;
import xa.miniproject.repositories.R_Biodata;

/**
 * C_GenerateToken
 */
@Controller
@RequestMapping("generate")
public class C_GenerateToken {
    @Autowired
    private R_Biodata rBiodata;

    @GetMapping("{id}")
    public ModelAndView formsendtoken(@PathVariable("id") long id){
        ModelAndView view = new ModelAndView("D_SendEmail/kirimtoken");
        M_Biodata biodata = this.rBiodata.findById(id).orElse(null);
        view.addObject("biodata", biodata);
        String TOKENCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * TOKENCHARS.length());
            salt.append(TOKENCHARS.charAt(index));
        }
        String token = salt.toString();
        view.addObject("token", token);
        return view;
    }

    @PostMapping("generated")
    public ModelAndView softdelete(@ModelAttribute M_Biodata biodata, BindingResult result, @RequestParam("token") String tokenString)
            throws InterruptedException {
        
        if (result.hasErrors()) {
            System.out.println("Here is error");
            System.out.println("Error are "+result);
            return new ModelAndView("redirect:/");
        } else {         
            System.out.println("Sleeping now...");
            Thread.sleep(10000);
            biodata.setDelete(false);
            biodata.setModifiedBy(new Long(1));
            biodata.setModifiedOn(new Date());
            biodata.setToken(tokenString);
            this.rBiodata.save(biodata);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
}
    
}
