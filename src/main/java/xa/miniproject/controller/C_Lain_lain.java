package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_Keterangan_Tambahan;
import xa.miniproject.models.M_Referensi;
import xa.miniproject.repositories.R_Keterangan_Tambahan;
import xa.miniproject.repositories.R_Referensi;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/D_Lain_lain/")
public class C_Lain_lain {
    @Autowired
    private R_Referensi r_referensi;

    @Autowired
    private R_Keterangan_Tambahan r_keterangan_tambahan;

    // Referensi
    @GetMapping(value = "lihat_ket_tamb_gabung/{id}")
    public ModelAndView lihat_ket_tamb_gabung(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_Lain_lain/lihat_ket_tamb_gabung");

        view.addObject("biodataId", id);

        List<M_Referensi> listreferensi = this.r_referensi.findByBioId(id);
        view.addObject("listreferensi", listreferensi);

        List<M_Keterangan_Tambahan> listlainnya = this.r_keterangan_tambahan.findByBioId(id);
        view.addObject("listlainnya", listlainnya);

        return view;
    }

    @GetMapping(value = "form_tambah_referensi")
    public ModelAndView form_tambah_referensi(){
        ModelAndView view = new ModelAndView("D_Lain_lain/form_tambah_referensi");
        view.addObject("formref", new M_Referensi());
        return view;
    }

    @GetMapping(value = "tampilkan_semua_tamb/{id}")
    public ModelAndView tampilkan_semua_tamb(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_Lain_lain/tampilkan_semua_tamb");
        M_Keterangan_Tambahan listlainnya = this.r_keterangan_tambahan.findById(id).orElse(null);
        view.addObject("listlainnya", listlainnya);
        return view;
    }

    @PostMapping(value = "saveref/{biodataId}")
    public ModelAndView saveref(@PathVariable ("biodataId") Long id, @ModelAttribute M_Referensi m_referensi, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("redirect:/D_Lain_lain/form_tambah_referensi");
        }
        else {
            m_referensi.setCreatedBy(new Long(1));
            m_referensi.setCreatedOn(new Date());
            m_referensi.setDelete(false);
            this.r_referensi.save(m_referensi);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    @PostMapping(value = "saverefmodif/{biodataId}")
    public ModelAndView saverefmodif(@PathVariable ("biodataId") Long id, @ModelAttribute M_Referensi m_referensi, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("redirect:/D_Lain_lain/form_edit_referensi");
        }
        else {
            m_referensi.setModifiedBy(new Long(1));
            m_referensi.setModifiedOn(new Date());
            m_referensi.setDelete(false);
            this.r_referensi.save(m_referensi);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    @GetMapping(value = "editform/{id}")
    public ModelAndView editform(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("/D_Lain_lain/form_edit_referensi");
        M_Referensi item = this.r_referensi.findById(id).orElse(null);
        view.addObject("formref", item);
        return view;
    }

    @GetMapping(value = "konfirm_hapus_referensi/{id}")
    public ModelAndView konfirm_hapus_referensi(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("/D_Lain_lain/konfirm_hapus_referensi");
        M_Referensi item = this.r_referensi.findById(id).orElse(null);
        view.addObject("formref", item);
        return view;
    }

    @PostMapping(value = "deleteref/{id}")
    public ModelAndView deleteref(@PathVariable("id") Long id, @ModelAttribute M_Referensi m_referensi) {
        this.r_referensi.delete(m_referensi);
        return new ModelAndView("redirect:/D_CariPelamar/view");
    }

    @GetMapping(value = "edittamb/{id}")
    public ModelAndView edittamb(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("/D_Lain_lain/ubah_ket_tamb_modif");
        M_Keterangan_Tambahan item = this.r_keterangan_tambahan.findById(id).orElse(null);
        view.addObject("formtamb", item);
        return view;
    }

    @GetMapping(value = "ubah_ket_tamb_modif/{id}")
    public ModelAndView ubah_ket_tamb_modif(@PathVariable ("id") Long id) {
        ModelAndView view = new ModelAndView("D_Lain_lain/ubah_ket_tamb_modif/" + id);
        view.addObject("formtamb", new M_Keterangan_Tambahan());
        return view;
    }

    @PostMapping(value = "savetambmodif/{biodataId}")
    public ModelAndView savetambmodif(@PathVariable ("biodataId") Long id, @ModelAttribute M_Keterangan_Tambahan m_keterangan_tambahan, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("redirect:/D_Lain_lain/ubah_ket_tamb_modif");
        }
        else {
            m_keterangan_tambahan.setCreatedBy(new Long(1));
            m_keterangan_tambahan.setModifiedBy(new Long(1));
            m_keterangan_tambahan.setModifiedOn(new Date());
            m_keterangan_tambahan.setDelete(false);
            this.r_keterangan_tambahan.save(m_keterangan_tambahan);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    @PostMapping(value = "is_delete/{biodataId}")
    public ModelAndView is_delete(@PathVariable ("biodataId") Long id, @ModelAttribute M_Referensi m_referensi, BindingResult result) {
        ModelAndView view = new ModelAndView("/D_Lain_lain/konfirm_hapus_referensi");
        M_Referensi item = this.r_referensi.findById(id).orElse(null);
        view.addObject("formref", item);

        List<M_Referensi> listreferensi = this.r_referensi.findByBioId(id);
        view.addObject("listreferensi", listreferensi);

        if (result.hasErrors()) {
            return new ModelAndView("redirect:/D_Lain_lain/konfirm_hapus_referensi");
        }
        else {
            m_referensi.setCreatedBy(new Long(1));
            m_referensi.setDelete(true);
            m_referensi.setDeletedBy(new Long(1));
            m_referensi.setDeletedOn(new Date());
            m_referensi.setModifiedBy(new Long(1));
            m_referensi.setModifiedOn(new Date());
            this.r_referensi.save(m_referensi);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    // API Referensi
    @GetMapping(value = "getbioRef/{id}")
    public List<M_Referensi> listRef(@PathVariable("id") Long id) {
        return this.r_referensi.findByBioId(id);
    }

    @GetMapping(value = "getRef/{id}")
    public M_Referensi getRef(@PathVariable("id") Long id) {
        return this.r_referensi.findById(id).orElse(null);
    }

    @PutMapping(value = "editRef/{id}")
    public M_Referensi editRef(@RequestBody M_Referensi item) {
        return this.r_referensi.save(item);
    }

    @PostMapping(value = "postRef/")
    public M_Referensi saveRef(@RequestBody M_Referensi item) { return this.r_referensi.save(item); }

    // API Lain-lain
    @GetMapping(value = "getbioTamb/{id}")
    public List<M_Keterangan_Tambahan> listTamb(@PathVariable("id") Long id) {
        return this.r_keterangan_tambahan.findByBioId(id);
    }

    @GetMapping(value = "getTamb/{id}")
    public M_Keterangan_Tambahan getTamb(@PathVariable("id") Long id) {
        return this.r_keterangan_tambahan.findById(id).orElse(null);
    }

    @PutMapping(value = "editTambAPI/{id}")
    public M_Keterangan_Tambahan editTambAPI(@RequestBody M_Keterangan_Tambahan item) {
        return this.r_keterangan_tambahan.save(item);
    }

}
