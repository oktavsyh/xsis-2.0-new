package xa.miniproject.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import xa.miniproject.models.M_EducationLevel;
import xa.miniproject.models.M_Family_Relation;
import xa.miniproject.models.M_Family_Tree_Type;
import xa.miniproject.models.M_Keluarga;
import xa.miniproject.repositories.R_EducationalLevel;
import xa.miniproject.repositories.R_FamilyRelation;
import xa.miniproject.repositories.R_FamilyTreeType;
import xa.miniproject.repositories.R_Keluarga;

@Controller
@RequestMapping("/D_Keluarga/")
public class C_Keluarga {

    @Autowired
    private R_Keluarga r_Keluarga;

    @Autowired
    private R_FamilyTreeType r_FamilyTreeType;

    @Autowired
    private R_FamilyRelation r_FamilyRelation;

    @Autowired
    private R_EducationalLevel r_EducationalLevel;

    // Lihat Keluarga
    @GetMapping("lihat_keluarga/{id}/{id2}")
    public ModelAndView lihat_keluarga(@PathVariable("id") Long id, @PathVariable("id2") Boolean id2) {
        ModelAndView view_keluarga = new ModelAndView("D_Keluarga/lihat_keluarga");

        List<M_Keluarga> list_keluarga = this.r_Keluarga.findByBioId(id, id2);

        view_keluarga.addObject("keluargaid", id);
        view_keluarga.addObject("listkeluarga", list_keluarga);
        return view_keluarga;
    }

    // Form Tambah Keluarga
    @GetMapping("form_tambah_keluarga")
    public ModelAndView form_tambah_keluarga() {
        ModelAndView view_keluarga = new ModelAndView("D_Keluarga/form_tambah_keluarga");
        view_keluarga.addObject("objkeluarga", new M_Keluarga());

        List<M_Family_Tree_Type> listfamilytreetype = this.r_FamilyTreeType.findAll();
        view_keluarga.addObject("listfamilytreetype", listfamilytreetype);

        // List<M_Family_Relation> listfamilyrelation = this.r_FamilyRelation.findAll();
        // view_keluarga.addObject("listfamilyrelation", listfamilyrelation);

        List<M_EducationLevel> listeducationlevel = this.r_EducationalLevel.findAll();
        view_keluarga.addObject("listeducationlevel", listeducationlevel);

        return view_keluarga;
    }

    // Form Family Relation
    @GetMapping("family_relation/{id}")
    public ModelAndView family_relation(@PathVariable("id") Long id) {
        ModelAndView view_keluarga = new ModelAndView("D_Keluarga/family_relation");
        view_keluarga.addObject("objkeluarga", new M_Keluarga());

        List<M_Family_Relation> listfamilyrelation = this.r_FamilyRelation.cari_hubungan(id);
        view_keluarga.addObject("listfamilyrelation", listfamilyrelation);

        return view_keluarga;
    }

    // Controller Untuk Save Setelah Form Tambah Keluarga
    @PostMapping("save")
    public ModelAndView save_keluarga(@ModelAttribute M_Keluarga keluarga, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            keluarga.setCreateBy(new Long(1));
            keluarga.setCreateOn(new Date());
            keluarga.setIsDelete(false);
            this.r_Keluarga.save(keluarga);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    // Form Edit #Tahap 1
    @GetMapping("edit/{id}")
    public ModelAndView edit_keluarga(@PathVariable("id") Long id) {
        ModelAndView view_keluarga = new ModelAndView("D_Keluarga/form_edit_keluarga");
        M_Keluarga item = this.r_Keluarga.findById(id).orElse(null);
        view_keluarga.addObject("objkeluarga", item);

        List<M_Family_Tree_Type> listfamilytreetype = this.r_FamilyTreeType.findAll();
        view_keluarga.addObject("listfamilytreetype", listfamilytreetype);

        List<M_Family_Relation> listfamilyrelation = this.r_FamilyRelation.findAll();
        view_keluarga.addObject("listfamilyrelation", listfamilyrelation);

        List<M_EducationLevel> listeducationlevel = this.r_EducationalLevel.findAll();
        view_keluarga.addObject("listeducationlevel", listeducationlevel);

        return view_keluarga;
    }

    // Controller Untuk Save Setelah Form Edit Keluarga #Tahap 2
    @PostMapping("save_edit")
    public ModelAndView save_edit(@ModelAttribute M_Keluarga keluarga, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            keluarga.setCreateBy(new Long(10));
            keluarga.setModifiedBy(new Long(10));
            keluarga.setModifiedOn(new Date());
            keluarga.setIsDelete(false);
            this.r_Keluarga.save(keluarga);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    // is_delete [Passing ke Form Hapus Keluarga] #Tahap 1
    @GetMapping("hapus/{id}")
    public ModelAndView hapus_keluarga(@PathVariable("id") Long id) {
        ModelAndView view_keluarga = new ModelAndView("D_Keluarga/form_hapus_keluarga");
        M_Keluarga item = this.r_Keluarga.findById(id).orElse(null);
        view_keluarga.addObject("objkeluarga", item);

        List<M_Family_Tree_Type> listfamilytreetype = this.r_FamilyTreeType.findAll();
        view_keluarga.addObject("listfamilytreetype", listfamilytreetype);

        List<M_Family_Relation> listfamilyrelation = this.r_FamilyRelation.findAll();
        view_keluarga.addObject("listfamilyrelation", listfamilyrelation);

        List<M_EducationLevel> listeducationlevel = this.r_EducationalLevel.findAll();
        view_keluarga.addObject("listeducationlevel", listeducationlevel);

        return view_keluarga;
    }

    // is_delete #Tahap 2 Untuk Hapus tanpa menghilangkan row DB
    @PostMapping("is_delete")
    public ModelAndView is_delete(@ModelAttribute M_Keluarga keluarga, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            // keluarga.setCreateBy(new Long(10));
            keluarga.setDeletedOn(new Date());
            keluarga.setDeletedBy(new Long(10));
            keluarga.setIsDelete(true);
            this.r_Keluarga.save(keluarga);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }
}