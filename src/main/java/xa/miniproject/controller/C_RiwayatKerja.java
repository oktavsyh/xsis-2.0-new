package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_RiwayatKerja;
import xa.miniproject.repositories.R_Biodata;
import xa.miniproject.repositories.R_RiwayatKerja;

import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/D_RiwayatKerja/")
public class C_RiwayatKerja {
    @Autowired
    private R_RiwayatKerja r_riwayatKerja;

    @Autowired
    private R_Biodata repoBio;

    @GetMapping(value = "lihat_riwayatKerja/{id}")
    public ModelAndView lihat_riwayatKerja(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("/D_RiwayatKerja/lihat_riwayatKerja");
        List<M_RiwayatKerja> kerjaList = this.r_riwayatKerja.findByBioId(id);
        view.addObject("kerjaList", kerjaList);
        view.addObject("biodataid", id);
        return view;
    }

    @GetMapping(value = "/getRK/{id}")
    public M_RiwayatKerja getRK(@PathVariable("id") Long id) {
        return this.r_riwayatKerja.findById(id).orElse(null);
    }

    @PutMapping(value = "/editRK/{id}")
    public M_RiwayatKerja updateRK(@RequestBody M_RiwayatKerja item) {
        return this.r_riwayatKerja.save(item);
    }

    @PostMapping(value = "/saveRK/")
    public M_RiwayatKerja saveRK(@RequestBody M_RiwayatKerja item){
        return this.r_riwayatKerja.save(item);
    }

    @GetMapping(value = "form")
    public ModelAndView form() {
        ModelAndView form = new ModelAndView("/D_RiwayatKerja/form");
        form.addObject("objrole", new M_RiwayatKerja());
        return form;
    }

    @PostMapping(value = "save")
    public ModelAndView save(@ModelAttribute M_RiwayatKerja kerja, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("redirect:/D_RiwayatKerja/form");
        } else {
            kerja.setCreatedBy(new Long(1));
            kerja.setCreatedOn(new Date());
            kerja.setDelete(false);
            this.r_riwayatKerja.save(kerja);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("/D_RiwayatKerja/form_edit");
        M_RiwayatKerja item = this.r_riwayatKerja.findById(id).orElse(null);
        view.addObject("objrole", item);
        return view;
    }

    @PostMapping(value = "save_edit")
    public ModelAndView save_edit(@ModelAttribute M_RiwayatKerja kerja, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("redirect:/D_RiwayatKerja/form_edit");
        } else {
            kerja.setModifiedBy(new Long(1));
            kerja.setModifiedOn(new Date());
            kerja.setDelete(false);
            this.r_riwayatKerja.save(kerja);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }

    @GetMapping(value = "hapus/{id}")
    public ModelAndView hapus(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("/D_RiwayatKerja/hapus");
        M_RiwayatKerja item = this.r_riwayatKerja.findById(id).orElse(null);
        view.addObject("objrole", item);
        return view;
    }

    @PostMapping("is_delete")
    public ModelAndView is_delete(@ModelAttribute M_RiwayatKerja kerja, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            kerja.setModifiedBy(new Long(1));
            kerja.setModifiedOn(new Date());
            kerja.setDeletedBy(new Long(1));
            kerja.setDeletedOn(new Date());
            kerja.setDelete(true);
            this.r_riwayatKerja.save(kerja);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }
}
