package xa.miniproject.controller;

import org.hibernate.internal.build.AllowPrintStacktrace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.*;
import xa.miniproject.repositories.*;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/D_TambahPelamar/")
public class C_TambahPelamar {
    private static final Logger LOGGER = LoggerFactory.getLogger(C_TambahPelamar.class);

    @Autowired
    private R_Biodata r_bio;
    @Autowired
    private R_Religion r_agama;
    @Autowired
    private R_IdentityType r_identityType;
    @Autowired
    private R_MaritalStatus r_maritalStatus;
    @Autowired
    private R_Address r_address;

    @GetMapping(value = "tambahpelamar")
    public ModelAndView tambahpelamar() {
        ModelAndView view = new ModelAndView("D_TambahPelamar/tambahpelamar");
        view.addObject("objbio", new M_Biodata());

        List<M_Religion> listagama = this.r_agama.findAll();
        view.addObject("listagama", listagama);

        List<M_IdentityType> listidentitytype = this.r_identityType.findAll();
        view.addObject("listidentitytype", listidentitytype);

        List<M_MaritalStatus> listmaritalstatus = this.r_maritalStatus.findAll();
        view.addObject("listmaritalstatus", listmaritalstatus);

        return view;
    }

    @PostMapping(value = "save")
    public ModelAndView save(@ModelAttribute M_Biodata bio, BindingResult result) {
        LOGGER.info("save : " + result);
        System.out.println(result);
        // String ph1 = result.getFieldValue("nomerHp1").toString();
        // String ph2 = result.getFieldValue("phoneNumber2").toString();
        // String ph3 = result.getFieldValue("parentPhoneNumber").toString();
        // String em =result.getFieldValue("Emaill").toString();
        // String idno = result.getFieldValue("identityNo").toString();
        // Long check = r_bio.validate(ph1, ph2, ph3, em, idno);
        // System.out.println(check);
        // if (check !=null) {

        if (result.hasErrors()) {
            System.out.println("erroooooooooor");
            System.out.println(result);
            return new ModelAndView();
        } else {
            LOGGER.info("dobDate : " + bio.getDob());
            bio.setCreatedBy(new Long(1));
            bio.setCreatedOn(new Date());
            bio.setDob(new Date());
            bio.setIdentityTypeId(new Long(1));
            // bio.setEmaill(new String(" "));
            // bio.setPhoneNumber1(new String(" "));
            // bio.setParentPhoneNumber(new String(" "));
            bio.setMaritalStatusId(new Long(1));
            bio.setCompanyId(new Long(1));
            bio.setDelete(false);
            try {
                this.r_bio.save(bio);
                Long max = r_bio.findBymaxid();
                ModelAndView view = new ModelAndView();
            } catch (Exception p) {
                p.printStackTrace();
                return new ModelAndView("redirect:/D_TambahPelamar/tambahpelamar");

            }

            return new ModelAndView("redirect:/D_TambahPelamar/tambahpelamar");
        }
    }

    @GetMapping(value = "view/{id}")
    public ModelAndView view(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_TambahPelamar/view");

        M_Biodata listbio = this.r_bio.findByBioId(id);
        view.addObject("listbio", listbio);

        List<M_Religion> listagama = this.r_agama.findAll();
        view.addObject("listagama", listagama);

        List<M_IdentityType> listidentitytype = this.r_identityType.findAll();
        view.addObject("listidentitytype", listidentitytype);

        List<M_MaritalStatus> listmaritalstatus = this.r_maritalStatus.findAll();
        view.addObject("listmaritalstatus", listmaritalstatus);

        return view;
    }

    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_TambahPelamar/edit");
        M_Biodata biodata = this.r_bio.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Bio ID :" + id));
        view.addObject("objbio", biodata);
        return view;
    }

    @PostMapping("update")
    public ModelAndView update(M_Biodata bio, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView();
        } else {
            LOGGER.info("dobDate : " + bio.getDob());
            bio.setCreatedBy(new Long(1));
            bio.setCreatedOn(new Date());
            bio.setDob(new Date());
            bio.setIdentityTypeId(new Long(1));
            // bio.setEmaill(new String(" "));
            // bio.setPhoneNumber1(new String(" "));
            // bio.setParentPhoneNumber(new String(" "));
            bio.setMaritalStatusId(new Long(1));
            bio.setCompanyId(new Long(1));
            bio.setDelete(false);
            try {
                this.r_bio.save(bio);
                Long max = r_bio.findBymaxid();
                ModelAndView view = new ModelAndView();
            } catch (Exception p) {
                return new ModelAndView("redirect:/D_TambahPelamar/view");

            }

            return new ModelAndView("redirect:/D_TambahPelamar/view");
        }
    }

    @GetMapping(value = "edit_biodata/{id}")
    public ModelAndView edit_biodata(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_TambahPelamar/edit_biodata");
        M_Biodata biodata = this.r_bio.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Bio ID :" + id));
        view.addObject("objbio", biodata);

        List<M_Religion> listagama = this.r_agama.findAll();
        view.addObject("listagama", listagama);

        List<M_IdentityType> listidentitytype = this.r_identityType.findAll();
        view.addObject("listidentitytype", listidentitytype);

        List<M_MaritalStatus> listmaritalstatus = this.r_maritalStatus.findAll();
        view.addObject("listmaritalstatus", listmaritalstatus);

        return view;
    }

}
