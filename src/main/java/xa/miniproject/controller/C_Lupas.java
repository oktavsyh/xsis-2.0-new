package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_Addrbook;
import xa.miniproject.repositories.R_Addrbook;

import java.util.List;

@RestController
@RequestMapping("/D_Lupas/")
public class C_Lupas {
    @Autowired
    private R_Addrbook r_addrbook;

    @GetMapping(value = "form_lupas")
    public ModelAndView form_lupas() {
        ModelAndView view = new ModelAndView("D_Lupas/form_lupas");
        return view;
    }

    @GetMapping(value = "form_lupas/{id}")
    public ModelAndView form_lupas(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_Lupas/form_lupas");
        M_Addrbook item = this.r_addrbook.findById(id).orElse(null);
        view.addObject("objlupas", item);
        return view;
    }

    @GetMapping(value = "validateemail/{string}")
    public Long validate(@PathVariable("string") String v1){return this.r_addrbook.validateemail(v1);}

    @GetMapping(value = "getAllLupas")
    public List<M_Addrbook> getAllLupas(){return this.r_addrbook.findAll();}

    @GetMapping(value = "getLupas/{id}")
    public M_Addrbook getLupas(@PathVariable("id") Long id) {
        return this.r_addrbook.findById(id).orElse(null);
    }

    @PutMapping(value = "editLupas/{email}")
    public M_Addrbook editLupas(@RequestBody M_Addrbook item) {
        return this.r_addrbook.save(item);
    }

    @GetMapping(value = "getdataAB/{iniemail}")
    public M_Addrbook getdataAB(@PathVariable("iniemail") String iniemail){return this.r_addrbook.getdataAB(iniemail);}

    @GetMapping(value = "lupas")
    public ModelAndView lupas(){
        ModelAndView view = new ModelAndView("D_Lupas/lupas");
        return view;
    }


}
