package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.*;
import xa.miniproject.repositories.R_Client;
import xa.miniproject.repositories.R_Placement;
import xa.miniproject.repositories.R_Timesheet;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/D_Timesheet/")
public class C_Timesheet {
    @Autowired
    private R_Timesheet r_timesheet;

    @Autowired
    private R_Placement r_placement;

    @Autowired
    private R_Client r_client;

    @GetMapping(value = "lihat_timesheet")
    public ModelAndView view(){
        ModelAndView view = new ModelAndView("/D_Timesheet/lihat_timesheet");
        List<M_Timesheet> timesheets = this.r_timesheet.findByBioId();
        view.addObject("List",timesheets);
        List<M_Placement> placements = this.r_placement.findByBioId();
        view.addObject("plist",placements);
        List<M_Client> clients = this.r_client.findAll();
        view.addObject("clist",clients);
        return view;
    }

    @GetMapping(value = "/getTS/{id}")
    public M_Timesheet getTS(@PathVariable("id") Long id) {
        return this.r_timesheet.findById(id).orElse(null);
    }

    @PostMapping(value = "/saveTS/")
    public M_Timesheet saveTS(@RequestBody M_Timesheet item){
        return this.r_timesheet.save(item);
    }

    @GetMapping(value = "/getCL/{id}")
    public M_Client getCL(@PathVariable("id") Long id) {
        return this.r_client.findById(id).orElse(null);
    }

    @GetMapping(value = "form")
    public ModelAndView form(){
        ModelAndView form = new ModelAndView("/D_Timesheet/form");
        form.addObject("objrole",new M_Timesheet());
        List<M_Placement> placements = this.r_placement.findByBioId();
        form.addObject("plist",placements);
        List<M_Client> clients = this.r_client.findAll();
        form.addObject("clist",clients);
        return form;
    }

    @PostMapping(value = "save")
    public ModelAndView save(@ModelAttribute M_Timesheet role , BindingResult result){
        if(result.hasErrors()){
            return new ModelAndView("redirect:/D_Timesheet/form");
        }
        else{
            role.setPlacementId(new Long(1));
            role.setSubmittedOn(new Date());
            role.setCreatedBy(new Long(1));
            role.setCreatedOn(new Date());
            role.setDelete(false);
            this.r_timesheet.save(role);
            return new ModelAndView("redirect:/D_Timesheet/lihat_timesheet");
        }
    }

    @GetMapping(value = "detail/{id}")
    public ModelAndView detail(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_Timesheet/detail");
        M_Timesheet item = this.r_timesheet.findById(id).orElse(null);
        view.addObject("item",item);
        return view;
    }

    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_Timesheet/form_edit");
        M_Timesheet item = this.r_timesheet.findById(id).orElse(null);
        view.addObject("objrole",item);
        List<M_Placement> placements = this.r_placement.findByBioId();
        view.addObject("plist",placements);
        List<M_Client> clients = this.r_client.findAll();
        view.addObject("clist",clients);
        return view;
    }

    @PostMapping(value = "save_edit")
    public ModelAndView save_edit(@ModelAttribute M_Timesheet role , BindingResult result){
        if(result.hasErrors()){
            return new ModelAndView("redirect:/D_Timesheet/form_edit");
        }
        else{
            role.setModifiedBy(new Long(1));
            role.setModifiedOn(new Date());
            this.r_timesheet.save(role);
            return new ModelAndView("redirect:/D_Timesheet/lihat_timesheet");
        }
    }

    @GetMapping(value = "view_by_date/{date1}/{date2}")
    public ModelAndView view_by_date(@PathVariable("date1") Date date1, @PathVariable("date2") Date date2){
        ModelAndView view = new ModelAndView("/D_Timesheet/lihat_timesheet");
        List<M_Timesheet> timesheets = this.r_timesheet.findByBetween(date1, date2);
        view.addObject("List",timesheets);
        List<M_Placement> client = this.r_placement.findByBioId();
        view.addObject("clist",client);
        return view;
    }

    @GetMapping(value="/valdate/{date}")
    public Long validate(@PathVariable("date") Date date1)
    {
        return this.r_timesheet.validateDate(date1);
    }


    @GetMapping(value = "hapus/{id}")
    public ModelAndView hapus(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_Timesheet/hapus");
        M_Timesheet item = this.r_timesheet.findById(id).orElse(null);
        view.addObject("objrole",item);
        List<M_Placement> placements = this.r_placement.findByBioId();
        view.addObject("plist",placements);
        List<M_Client> clients = this.r_client.findAll();
        view.addObject("clist",clients);
        return view;
    }

    @PostMapping("is_delete")
    public ModelAndView is_delete(@ModelAttribute M_Timesheet role, BindingResult result) {
        if (result.hasErrors()) {
            return null;
        } else {
            role.setModifiedBy(new Long(1));
            role.setModifiedOn(new Date());
            role.setDeletedBy(new Long(1));
            role.setDeletedOn(new Date());
            role.setDelete(true);
            this.r_timesheet.save(role);
            return new ModelAndView("redirect:/D_Timesheet/lihat_timesheet");
        }
    }

}
