package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_Biodata;
import xa.miniproject.models.M_EducationLevel;
import xa.miniproject.models.M_RiwayatPendidikan;
import xa.miniproject.repositories.R_Biodata;
import xa.miniproject.repositories.R_EducationalLevel;
import xa.miniproject.repositories.R_RiwayatPendidikan;

import java.util.List;

@RestController
@RequestMapping(value = "/D_RiwayatPendidikan/")
public class C_RiwayatPendidikan {
    @Autowired
    private R_RiwayatPendidikan repoRP;
    @Autowired
    private R_Biodata repoBio;
    @Autowired
    private R_EducationalLevel repoEL;

    @GetMapping(value = "view/{id}")
    public ModelAndView lihat(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_RiwayatPendidikan/view");
        List<M_RiwayatPendidikan> itemRP = this.repoRP.findByBioId(id);
        view.addObject("biodataid", id);
        view.addObject("listRP", itemRP);
        return view;
    }

    // Buat tambah data baru
    @GetMapping(value = "form")
    public ModelAndView form() {
        ModelAndView form = new ModelAndView("/D_RiwayatPendidikan/form");
        form.addObject("objRP", new M_RiwayatPendidikan());
        List<M_Biodata> listbio = this.repoBio.findAll();
        form.addObject("listbio", listbio);
        List<M_EducationLevel> listedu = this.repoEL.findAll();
        form.addObject("listedu", listedu);
        return form;
    }

    // Buat edit data yang ada
    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("/D_RiwayatPendidikan/edit");
        M_RiwayatPendidikan item = this.repoRP.findById(id).orElse(null);
        view.addObject("objRP", item);
        List<M_Biodata> listbio = this.repoBio.findAll();
        view.addObject("listbio", listbio);
        List<M_EducationLevel> listedu = this.repoEL.findAll();
        view.addObject("listedu", listedu);
        return view;
    }

    @GetMapping(value = "/getbioRP/{id}")
    public List<M_RiwayatPendidikan> list(@PathVariable("id") Long id) {
        return this.repoRP.findByBioId(id);
    }

    @GetMapping(value = "/getRP/{id}")
    public M_RiwayatPendidikan getRP(@PathVariable("id") Long id) {
        return this.repoRP.findById(id).orElse(null);
    }

    @PostMapping(value = "/saveRP/")
    public M_RiwayatPendidikan saveRP(@RequestBody M_RiwayatPendidikan item) {
        return this.repoRP.save(item);
    }

    @PutMapping(value = "/editRP/{id}")
    public M_RiwayatPendidikan editRP(@RequestBody M_RiwayatPendidikan item) {
        return this.repoRP.save(item);
    }
}
