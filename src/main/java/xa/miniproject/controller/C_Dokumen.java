package xa.miniproject.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import xa.miniproject.models.M_Dokumen;
import xa.miniproject.repositories.R_Dokumen;

@Controller
@RequestMapping("/D_Dokumen/")
public class C_Dokumen {
    private static final String UPLOADED_FOLDER = "./uploaded/";

    @Autowired
    private R_Dokumen r_dokumen;

    @GetMapping(value = "lihat_dokumen_gabung/{id}")
    public ModelAndView lihat_dokumen_gabung(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("D_Dokumen/lihat_dokumen_gabung");

        List<M_Dokumen> listdokumen = this.r_dokumen.findByBioId(id);
        view.addObject("biodataId", id);
        view.addObject("listdokumen", listdokumen);
        return view;
    }

    // Tampilkan Form Tambah
    @GetMapping(value = "form_tambah_dokumen_gabung")
    public ModelAndView form_tambah_dokumen_gabung() {
        ModelAndView view = new ModelAndView("D_Dokumen/form_tambah_dokumen_gabung");
        view.addObject("formupload", new M_Dokumen());
        view.addObject("formfoto", new M_Dokumen());
        return view;
    }

    // Upload File
    @PostMapping("upload_disini/{id}")
    public String singleFileUpload(@PathVariable("id") Long id, @RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
                                   @ModelAttribute M_Dokumen m_dokumen, BindingResult result, Model model) throws IOException {

        if (file.isEmpty()) {
            m_dokumen.setCreatedBy(new Long(1));
            m_dokumen.setCreatedOn(new Date());
            m_dokumen.setFilePath("/uploaded/" + file.getOriginalFilename());
            m_dokumen.setDelete(false);
            m_dokumen.setIsPhoto("false");

            this.r_dokumen.save(m_dokumen);
            return "redirect:/D_CariPelamar/view";
        }

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);

            m_dokumen.setCreatedBy(new Long(1));
            m_dokumen.setCreatedOn(new Date());
            m_dokumen.setFilePath("/uploaded/" + file.getOriginalFilename());
            m_dokumen.setDelete(false);
            m_dokumen.setIsPhoto("false");

            if (result.hasErrors()) {
                return "redirect:/D_CariPelamar/view";
            } else {
                this.r_dokumen.save(m_dokumen);
            }


        return "redirect:/D_CariPelamar/view";
    }

    @PostMapping("upload_foto/{id}")
    public String singleFotoUpload(@PathVariable("id") Long id, @RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
                                   @ModelAttribute M_Dokumen m_dokumen, BindingResult result, Model model) throws IOException {

        if (file.isEmpty()) {
            m_dokumen.setCreatedBy(new Long(1));
            m_dokumen.setCreatedOn(new Date());
            m_dokumen.setFilePath("/uploaded/" + file.getOriginalFilename());
            m_dokumen.setDelete(false);
            m_dokumen.setIsPhoto("true");

            this.r_dokumen.save(m_dokumen);
            return "redirect:/D_CariPelamar/view";
        }

        // Get the file and save it somewhere
        byte[] bytes = file.getBytes();
        Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
        Files.write(path, bytes);

        m_dokumen.setCreatedBy(new Long(1));
        m_dokumen.setCreatedOn(new Date());
        m_dokumen.setFilePath("/uploaded/" + file.getOriginalFilename());
        m_dokumen.setDelete(false);
        m_dokumen.setIsPhoto("true");

        if (result.hasErrors()) {
            return "redirect:/D_CariPelamar/view";
        } else {
            this.r_dokumen.save(m_dokumen);
        }


        return "redirect:/D_CariPelamar/view";
    }

    @GetMapping(value = "uploadStatus")
    public ModelAndView uploadStatus() {
        ModelAndView view = new ModelAndView("D_Dokumen/uploadStatus");

        List<M_Dokumen> fileList = this.r_dokumen.findAll();
        view.addObject("fileList", fileList);

        File[] files = new File(UPLOADED_FOLDER).listFiles();
        view.addObject("allfiles", files);

        return view;
    }

    @GetMapping(value = "edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_Dokumen/form_tambah_dokumen_gabung");
        M_Dokumen item = this.r_dokumen.findById(id).orElse(null);
        view.addObject("formupload", item);
        view.addObject("formket", item);
        view.addObject("formfoto", item);
        return view;
    }

    @GetMapping(value = "editmodif/{id}")
    public ModelAndView editmodif(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_Dokumen/form_edit_dokumen_gabung");
        M_Dokumen item = this.r_dokumen.findById(id).orElse(null);
        view.addObject("formupload", item);
        view.addObject("formket", item);
        view.addObject("formfoto", item);
        return view;
    }

    @GetMapping(value = "konfirm_hapus_dokumen/{id}")
    public ModelAndView konfirm_hapus_dokumen(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_Dokumen/konfirm_hapus_dokumen");
        M_Dokumen item = this.r_dokumen.findById(id).orElse(null);
        view.addObject("objdokumen",item);
        return view;
    }

    @PostMapping(value = "delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id, @ModelAttribute M_Dokumen m_dokumen){
        this.r_dokumen.delete(m_dokumen);
        return new ModelAndView("redirect:/D_CariPelamar/view");
    }

    // Is Delete File
    @PostMapping("is_delete_file/{id}")
    public String is_delete_file(@PathVariable("id") Long id, @ModelAttribute M_Dokumen m_dokumen, BindingResult result, Model model) throws IOException {

        m_dokumen.setCreatedBy(new Long(1));
        m_dokumen.setDelete(true);
        m_dokumen.setDeletedBy(new Long(1));
        m_dokumen.setDeletedOn(new Date());
        m_dokumen.setModifiedBy(new Long(1));
        m_dokumen.setModifiedOn(new Date());

        if (result.hasErrors()) {
            return "redirect:/D_CariPelamar/view";
        } else {
            this.r_dokumen.save(m_dokumen);
        }

        return "redirect:/D_CariPelamar/view";
    }

    // Edit File
    @PostMapping("edit_upload_file/{id}")
    public String edit_upload_file(@PathVariable("id") Long id, @ModelAttribute M_Dokumen m_dokumen, BindingResult result, Model model) throws IOException {

        m_dokumen.setModifiedBy(new Long(1));
        m_dokumen.setModifiedOn(new Date());
        m_dokumen.setIsPhoto("false");

        if (result.hasErrors()) {
            return "redirect:/D_CariPelamar/view";
        } else {
            this.r_dokumen.save(m_dokumen);
        }

        return "redirect:/D_CariPelamar/view";
    }

    // Edit Photo
    @PostMapping("edit_upload_photo/{id}")
    public String edit_upload_photo(@PathVariable("id") Long id, @RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
                                    @ModelAttribute M_Dokumen m_dokumen, BindingResult result, Model model) throws IOException {
        m_dokumen.setModifiedBy(new Long(1));
        m_dokumen.setModifiedOn(new Date());
        m_dokumen.setIsPhoto("true");

        m_dokumen.setFileName(file.getOriginalFilename());
        m_dokumen.setFilePath("/uploaded/" + file.getOriginalFilename());

        if (result.hasErrors()) {
            return "redirect:/D_CariPelamar/view";
        } else {
            this.r_dokumen.save(m_dokumen);
        }

        return "redirect:/D_CariPelamar/view";
    }
}
