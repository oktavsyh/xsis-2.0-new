package xa.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.miniproject.models.M_Biodata;
import xa.miniproject.models.M_Sumber_Loker;
import xa.miniproject.repositories.R_Biodata;
import xa.miniproject.repositories.R_Sumber_Loker;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/D_Sumberloker/")
public class C_Sumber_Loker{
    @Autowired
    private R_Sumber_Loker RS;

    @Autowired
    private R_Biodata RB;


    @GetMapping(value = "form/{id}")
    public ModelAndView form(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("/D_Sumberloker/form");
        view.addObject("objs",new M_Sumber_Loker());
        M_Sumber_Loker biodata = this.RS.findByBioId(id);
        view.addObject("biodidslk", id);
        view.addObject("listbs", biodata);
        return view;
    }

    @PostMapping(value = "save")
    public ModelAndView save(@ModelAttribute M_Sumber_Loker slk, BindingResult result){
        if(result.hasErrors()){
            return new ModelAndView("redirect:/D_Sumberloker/form");
        }else{
            slk.setCreatedOn(new Date());
            slk.setCreatedBy(new Long(1));
            slk.setDelete(false);
            this.RS.save(slk);
            return new ModelAndView("redirect:/D_CariPelamar/view");
        }
    }
}
