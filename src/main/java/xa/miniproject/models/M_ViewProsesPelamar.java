package xa.miniproject.models;

import javax.persistence.*;

@Entity
@Table(name="view_prosespelamar")
public class M_ViewProsesPelamar {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id")
    private Long Id;

    @Column(name = "fullname")
    private String fullName;

    @Column(name = "nick_name")
    private String nickName;

    @Column(name = "email")
    private String Email;

    @Column(name = "is_delete", nullable = false)
    private boolean isDelete;

    @Column(name = "is_process", nullable = true)
    private boolean isProcess;

    @Column(name = "is_complete", nullable = true)
    private boolean isComplete;

    @Column(name = "phone_number1")
    private Long phoneNumber1;

    @Column(name = "major")
    private String Major;

    @Column(name = "school_name")
    private String schoolName;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public boolean isProcess() {
        return isProcess;
    }

    public void setProcess(boolean process) {
        isProcess = process;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public Long getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(Long phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getMajor() {
        return Major;
    }

    public void setMajor(String major) {
        Major = major;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
}
