package xa.miniproject.models;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "x_sumber_loker")
public class M_Sumber_Loker {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long Id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", nullable = true, length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on", nullable = true)
    private Date modifiedOn;

    @Column(name = "deleted_by", nullable = true, length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on", nullable = true)
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private boolean isDelete;

    @Column(name = "biodata_id",nullable = false, length = 11)
    private Long biodataId;

    @Column(name = "vacancy_source", nullable = true, length = 20)
    private String vacancySource;

    @Column(name = "candidate_type", nullable = true, length = 10)
    private String candidateType;

    @Column(name = "event_name", nullable = true, length = 100)
    private String eventName;

    @Column(name = "career_center_name", nullable = true, length = 100)
    private String careerCenterName;

    @Column(name = "referrer_name", nullable = true, length = 100)
    private String referrerName;

    @Column(name = "referrer_phone", nullable = true, length = 20)
    private String referrerPhone;

    @Column(name = "referrer_email", nullable = true, length = 100)
    private String referrerEmail;

    @Column(name = "other_source", nullable = true, length = 100)
    private String otherSource;

    @Column(name = "last_income", nullable = true, length = 20)
    private String lastIncome;

    @Column(name = "apply_date", nullable = true)
    private Date applyDate;

//    @ManyToOne
//    @JoinColumn(name = "biodata_id",
//            foreignKey = @ForeignKey(name = "fe_biodata_id"),
//            insertable = false,
//            updatable = false
//    )
//    public M_Biodata biodata;
//
//    public M_Organisasi(){
//
//    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public Long getBiodataId() {
        return biodataId;
    }

    public void setBiodataId(Long biodataId) {
        this.biodataId = biodataId;
    }

    public String getVacancySource() {
        return vacancySource;
    }

    public void setVacancySource(String vacancySource) {
        this.vacancySource = vacancySource;
    }

    public String getCandidateType() {
        return candidateType;
    }

    public void setCandidateType(String candidateType) {
        this.candidateType = candidateType;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getCareerCenterName() {
        return careerCenterName;
    }

    public void setCareerCenterName(String careerCenterName) {
        this.careerCenterName = careerCenterName;
    }

    public String getReferrerName() {
        return referrerName;
    }

    public void setReferrerName(String referrerName) {
        this.referrerName = referrerName;
    }

    public String getReferrerPhone() {
        return referrerPhone;
    }

    public void setReferrerPhone(String referrerPhone) {
        this.referrerPhone = referrerPhone;
    }

    public String getReferrerEmail() {
        return referrerEmail;
    }

    public void setReferrerEmail(String referrerEmail) {
        this.referrerEmail = referrerEmail;
    }

    public String getOtherSource() {
        return otherSource;
    }

    public void setOtherSource(String otherSource) {
        this.otherSource = otherSource;
    }

    public String getLastIncome() {
        return lastIncome;
    }

    public void setLastIncome(String lastIncome) {
        this.lastIncome = lastIncome;
    }

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

//    public M_Biodata getBiodata() {
//        return biodata;
//    }
//
//    public void setBiodata(M_Biodata biodata) {
//        this.biodata = biodata;
//    }
}

