package xa.miniproject.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "x_catatan")
public class M_Catatan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long Id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", nullable = true, length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on", nullable = true)
    private Date modifiedOn;

    @Column(name = "deleted_by", nullable = true, length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on", nullable = true)
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private boolean isDelete;

    @Column(name = "biodata_id", nullable = false, length = 11)
    private long biodataId;

    @Column(name = "title", nullable = true, length = 100)
    private String title;

    @Column(name = "note_type_id", nullable = true, length = 11)
    private long noteTypeId;

    @Column(name = "notes", nullable = true, length = 1000)
    private String notes;

    @ManyToOne
    @JoinColumn(name = "note_type_id",
            foreignKey = @ForeignKey(name = "fe_note_type_id"),
            insertable = false,
            updatable = false
    )
    public M_Note note;

    public M_Catatan(){

    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public long getBiodataId() {
        return biodataId;
    }

    public void setBiodataId(long biodataId) {
        this.biodataId = biodataId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getNoteTypeId() {
        return noteTypeId;
    }

    public void setNoteTypeId(long noteTypeId) {
        this.noteTypeId = noteTypeId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public M_Note getNote() {
        return note;
    }

    public void setNote(M_Note note) {
        this.note = note;
    }
}
