package xa.miniproject.models;

import javax.persistence.*;


@Entity
@Table(name="x_homelogin")
public class M_login {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column (name="id", nullable = false)
    private Long Id;
    @Column (name="name", nullable = false)
    private String Name;
    @Column (name="username", nullable = false)
    private String Username;
    @Column (name="keylogin", nullable = false)
    private String keyloString;

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getName() {
        return this.Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getUsername() {
        return this.Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getKeyloString() {
        return this.keyloString;
    }

    public void setKeyloString(String keyloString) {
        this.keyloString = keyloString;
    }

}
