package xa.miniproject.models;

import javax.persistence.*;

@Entity
@Table(name = "x_ro")
public class M_RO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name="id", nullable=false)
    private Long id;

    @Column(name = "ro_name", nullable = false)
    private String roName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoName() {
        return roName;
    }

    public void setRoName(String roName) {
        this.roName = roName;
    }
}
