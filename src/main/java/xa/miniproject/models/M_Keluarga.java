package xa.miniproject.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "x_keluarga")
public class M_Keluarga {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false)
    private Long Id;

    @Column(name = "create_by", nullable = false)
    private Long createBy;

    @Column(name = "create_on", nullable = false)
    private Date createOn;

    @Column(name = "modified_by", nullable = true)
    private Long modifiedBy;

    @Column(name = "modified_on", nullable = true)
    private Date modifiedOn;

    @Column(name = "deleted_by", nullable = true)
    private Long deletedBy;

    @Column(name = "deleted_on", nullable = true)
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private boolean isDelete;

    @Column(name = "biodata_id", nullable = false)
    private Long biodataId;

    @Column(name = "family_tree_type_id", nullable = true)
    private Long familyTreeTypeId;

    @Column(name = "family_relation_id", nullable = true)
    private Long familyRelationId;

    @Column(name = "name", nullable = true)
    private String name;

    @Column(name = "gender", nullable = false)
    private boolean genderKeluarga;

    @Column(name = "dob", nullable = true)
    private Date dob;

    @Column(name = "education_level_id", nullable = true)
    private Long educationLevelId;

    @Column(name = "job", nullable = true)
    private String job;

    @Column(name = "notes", nullable = true)
    private String notes;

    @ManyToOne
    @JoinColumn(name = "family_relation_id", foreignKey = @ForeignKey(name = "fk_family_relation_id"), insertable = false, updatable = false)
    public M_Family_Relation m_Family_Relation;

    @ManyToOne
    @JoinColumn(name = "biodata_id", foreignKey = @ForeignKey(name = "fk_biodata_id"), insertable = false, updatable = false)
    public M_Biodata m_biodata;

    @ManyToOne
    @JoinColumn(name = "family_tree_type_id", foreignKey = @ForeignKey(name = "fe_family_tree_type_id"), insertable = false, updatable = false)
    public M_Family_Tree_Type m_Family_Tree_Type;

    @ManyToOne
    @JoinColumn(name = "education_level_id", foreignKey = @ForeignKey(name = "fkey_education_level_id"), insertable = false, updatable = false)
    public M_EducationLevel m_EducationLevel;

    public M_Keluarga() {

    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Long getCreateBy() {
        return this.createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateOn() {
        return this.createOn;
    }

    public void setCreateOn(Date createOn) {
        this.createOn = createOn;
    }

    public Long getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return this.deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return this.deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public boolean isIsDelete() {
        return this.isDelete;
    }

    public boolean getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Long getBiodataId() {
        return this.biodataId;
    }

    public void setBiodataId(Long biodataId) {
        this.biodataId = biodataId;
    }

    public Long getFamilyTreeTypeId() {
        return this.familyTreeTypeId;
    }

    public void setFamilyTreeTypeId(Long familyTreeTypeId) {
        this.familyTreeTypeId = familyTreeTypeId;
    }

    public Long getFamilyRelationId() {
        return this.familyRelationId;
    }

    public void setFamilyRelationId(Long familyRelationId) {
        this.familyRelationId = familyRelationId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGenderKeluarga() {
        return this.genderKeluarga;
    }

    public boolean getGenderKeluarga() {
        return this.genderKeluarga;
    }

    public void setGenderKeluarga(boolean genderKeluarga) {
        this.genderKeluarga = genderKeluarga;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Long getEducationLevelId() {
        return this.educationLevelId;
    }

    public void setEducationLevelId(Long educationLevelId) {
        this.educationLevelId = educationLevelId;
    }

    public String getJob() {
        return this.job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public M_Biodata getM_biodata() {
        return this.m_biodata;
    }

    public void setM_biodata(M_Biodata m_biodata) {
        this.m_biodata = m_biodata;
    }

    public M_Family_Tree_Type getM_Family_Tree_Type() {
        return this.m_Family_Tree_Type;
    }

    public void setM_Family_Tree_Type(M_Family_Tree_Type m_Family_Tree_Type) {
        this.m_Family_Tree_Type = m_Family_Tree_Type;
    }

    public M_EducationLevel getM_EducationLevel() {
        return this.m_EducationLevel;
    }

    public void setM_EducationLevel(M_EducationLevel m_EducationLevel) {
        this.m_EducationLevel = m_EducationLevel;
    }

}