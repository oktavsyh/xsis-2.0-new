package xa.miniproject.models;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "x_rencana_jadwal_detail")
public class M_Rencana_Jadwal_Detail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", nullable = true, length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on", nullable = true)
    private Date modifiedOn;

    @Column(name = "deleted_by", nullable = true, length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on", nullable = true)
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private boolean isDelete;

    @Column(name = "rencana_jadwal_id", nullable = false, length = 11)
    private Long rencanaJadwalId;
    @ManyToOne
    @JoinColumn(
            name = "rencana_jadwal_id",
            foreignKey = @ForeignKey(name = "fk_rencana_jadwal_id"),
            insertable = false,
            updatable = false
    )
    public M_Rencana_Jadwal rencjadwal;

    @Column(name = "biodata_id", nullable = false, length = 11)
    private Long biodataId;
    @ManyToOne
    @JoinColumn(
            name = "biodata_id",
            foreignKey = @ForeignKey(name = "fk_biodata_id"),
            insertable = false,
            updatable = false
    )
    public M_ViewProsesPelamar biojadwal;

    public M_Rencana_Jadwal_Detail(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public Long getRencanaJadwalId() {
        return rencanaJadwalId;
    }

    public void setRencanaJadwalId(Long rencanaJadwalId) {
        this.rencanaJadwalId = rencanaJadwalId;
    }

    public Long getBiodataId() {
        return biodataId;
    }

    public void setBiodataId(Long biodataId) {
        this.biodataId = biodataId;
    }
}
