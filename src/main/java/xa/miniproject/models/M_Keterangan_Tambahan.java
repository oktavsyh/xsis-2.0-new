package xa.miniproject.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "x_keterangan_tambahan")
public class M_Keterangan_Tambahan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on")
    private Date modifiedOn;

    @Column(name = "deleted_by", length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on")
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private boolean isDelete;

    @Column(name = "biodata_id", nullable = false, length = 11)
    private Long biodataId;

    @ManyToOne
    @JoinColumn(
            name = "biodata_id",
            foreignKey = @ForeignKey(name ="fk_biodata_id_tambahan"),
            insertable = false,
            updatable = false
    )

    public M_Biodata m_biodata;

    public M_Keterangan_Tambahan() {

    }

    @Column(name = "emergency_contact_name", length = 100)
    private String emergencyContactName;

    @Column(name = "emergency_contact_phone", length = 50)
    private String emergencyContactPhone;

    @Column(name = "expected_salary", length = 20)
    private String expectedSalary;

    @Column(name = "is_negotiable")
    public boolean isNegotiable;

    @Column(name = "start_working", length = 100)
    private String startWorking;

    @Column(name = "is_ready_to_outoftown")
    public boolean isReadyToOutoftown;

    @Column(name = "is_apply_other_place")
    public boolean isApplyOtherPlace;

    @Column(name = "apply_place", length = 100)
    private String applyPlace;

    @Column(name = "selection_phase", length = 100)
    private String selectionPhase;

    @Column(name = "is_ever_badly_sick")
    public boolean isEverBadlySick;

    @Column(name = "disease_name", length = 100)
    private String diseaseName;

    @Column(name = "disease_time", length = 100)
    private String diseaseTime;

    @Column(name = "is_ever_psychotest")
    public boolean isEverPsychotest;

    @Column(name = "psychotest_needs", length = 100)
    private String psychotestNeeds;

    @Column(name = "psychotest_time", length = 100)
    private String psychotestTime;

    @Column(name = "requirementes_required", length = 500)
    private String requirementesRequired;

    @Column(name = "other_notes", length = 1000)
    public String otherNotes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public Long getBiodataId() {
        return biodataId;
    }

    public void setBiodataId(Long biodataId) {
        this.biodataId = biodataId;
    }

    public String getEmergencyContactName() {
        return emergencyContactName;
    }

    public void setEmergencyContactName(String emergencyContactName) {
        this.emergencyContactName = emergencyContactName;
    }

    public String getEmergencyContactPhone() {
        return emergencyContactPhone;
    }

    public void setEmergencyContactPhone(String emergencyContactPhone) {
        this.emergencyContactPhone = emergencyContactPhone;
    }

    public String getExpectedSalary() {
        return expectedSalary;
    }

    public void setExpectedSalary(String expectedSalary) {
        this.expectedSalary = expectedSalary;
    }

    public boolean getIsNegotiable() {
        return isNegotiable;
    }

    public void setIsNegotiable(boolean isNegotiable) {
        this.isNegotiable = isNegotiable;
    }

    public String getStartWorking() {
        return startWorking;
    }

    public void setStartWorking(String startWorking) {
        this.startWorking = startWorking;
    }

    public boolean getIsReadyToOutoftown() {
        return isReadyToOutoftown;
    }

    public void setIsReadyToOutoftown(boolean isReadyToOutoftown) {
        this.isReadyToOutoftown = isReadyToOutoftown;
    }

    public boolean getIsApplyOtherPlace() {
        return isApplyOtherPlace;
    }

    public void setIsApplyOtherPlace(boolean isApplyOtherPlace) {
        this.isApplyOtherPlace = isApplyOtherPlace;
    }

    public String getApplyPlace() {
        return applyPlace;
    }

    public void setApplyPlace(String applyPlace) {
        this.applyPlace = applyPlace;
    }

    public String getSelectionPhase() {
        return selectionPhase;
    }

    public void setSelectionPhase(String selectionPhase) {
        this.selectionPhase = selectionPhase;
    }

    public boolean getIsEverBadlySick() {
        return isEverBadlySick;
    }

    public void setIsEverBadlySick(boolean isEverBadlySick) {
        this.isEverBadlySick = isEverBadlySick;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getDiseaseTime() {
        return diseaseTime;
    }

    public void setDiseaseTime(String diseaseTime) {
        this.diseaseTime = diseaseTime;
    }

    public boolean getIsEverPsychotest() {
        return isEverPsychotest;
    }

    public void setIsEverPsychotest(boolean isEverPsychotest) {
        this.isEverPsychotest = isEverPsychotest;
    }

    public String getPsychotestNeeds() {
        return psychotestNeeds;
    }

    public void setPsychotestNeeds(String psychotestNeeds) {
        this.psychotestNeeds = psychotestNeeds;
    }

    public String getPsychotestTime() {
        return psychotestTime;
    }

    public void setPsychotestTime(String psychotestTime) {
        this.psychotestTime = psychotestTime;
    }

    public String getRequirementesRequired() {
        return requirementesRequired;
    }

    public void setRequirementesRequired(String requirementesRequired) {
        this.requirementesRequired = requirementesRequired;
    }

    public String getOtherNotes() {
        return otherNotes;
    }

    public void setOtherNotes(String otherNotes) {
        this.otherNotes = otherNotes;
    }
}
