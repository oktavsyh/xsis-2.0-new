package xa.miniproject.models;

import java.util.Date;

import javax.persistence.*;
/**
 * M_Religion
 */
@Entity
@Table(name="x_religion")
public class M_Religion {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name = "id",length = 11, nullable = false)
    private Long id;


    @Column(name = "created_by",length = 11,nullable = false)
    private Long createdBy;
    
    @Column(name = "created_on",nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on")
    private Date modifiedOn;

    @Column(name = "deleted_by", length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on")
    private Date DeletedOn;

    @Column(name = "is_delete", nullable = false)
    private Boolean isDelete;

    @Column(name = "name",length = 50,nullable = false)
    private String name;

    @Column(name = "description",length = 100)
    private String description;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return this.deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return this.DeletedOn;
    }

    public void setDeletedOn(Date DeletedOn) {
        this.DeletedOn = DeletedOn;
    }

    public Boolean isIsDelete() {
        return this.isDelete;
    }

    public Boolean getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}