package xa.miniproject.models;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "x_biodata", uniqueConstraints = {@UniqueConstraint(columnNames = {"parent_phone_number", "identity_no", "email", "phone_number2", "phone_number1"})})
public class M_Biodata {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long Id;

    @OneToMany
            (cascade = CascadeType.ALL,
            fetch = FetchType.LAZY )
    private Set<M_RiwayatPendidikan> tabelRP = new HashSet<>();

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", nullable = true, length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on", nullable = true)
    private Date modifiedOn;

    @Column(name = "deleted_by", nullable = true, length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on", nullable = true)
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private boolean isDelete;

    @Column(name = "fullname", nullable = false, length = 255)
    private String fullName;

    @Column(name = "nick_name", nullable = false, length = 100)
    private String nickName;

    @Column(name = "pob", nullable = false, length = 100)
    private String pob;

    @Column(name = "dob", nullable = false)
    private Date dob;

    @Column(name = "gender", nullable = false)
    private boolean gender;

    @ManyToOne
    @JoinColumn(name="religion_id",
            foreignKey = @ForeignKey(name = "fo_religion_id"),
            insertable = false,
            updatable = false
    )
    public M_Religion religion;

    public M_Biodata(){

    }

    @Column(name = "religion_id", nullable = false, length = 11)
    private Long religionId;

    @Column(name = "high", nullable = true)
    private Integer high;

    @Column(name = "weight", nullable = true)
    private Integer weight;

    @Column(name = "nationality", nullable = true, length = 100)
    private String nationality;

    @Column(name = "ethnic", nullable = true, length = 50)
    private String ethnic;

    @Column(name = "hobby", nullable = true, length = 255)
    private String hobby;

    @ManyToOne
    @JoinColumn(name="identity_type_id",
            foreignKey = @ForeignKey(name = "fo_identity_type_id"),
            insertable = false,
            updatable = false
    )
    public M_IdentityType identity;

    @Column(name = "identity_type_id", nullable = false, length = 11)
    private Long identityTypeId;

    @Column(name = "identity_no", nullable = false, length = 50)
    private String identityNo;

    @Column(name = "email", nullable = false, length = 100)
    private String email;

    @Column(name = "phone_number1", nullable = false, length = 50)
    private String phoneNumber1;

    @Column(name = "phone_number2", nullable = true, length = 50)
    private String phoneNumber2;

    @Column(name = "parent_phone_number", nullable = false, length = 50)
    private String parentPhoneNumber;

    @Column(name = "child_sequence", nullable = true, length = 5)
    private String childSequence;

    @Column(name = "how_many_brothers", nullable = true, length = 5)
    private String howManyBrothers;

    @ManyToOne
    @JoinColumn(name="marital_status_id",
            foreignKey = @ForeignKey(name = "fo_marital_status_id"),
            insertable = false,
            updatable = false
    )
    public M_MaritalStatus marital;

    @Column(name = "marital_status_id", nullable = false, length = 11)
    private Long maritalStatusId;

    @ManyToOne
    @JoinColumn(name="addrbook_id",
            foreignKey = @ForeignKey(name = "fo_addrbook_id"),
            insertable = false,
            updatable = false
    )
    public M_Addrbook addrs;

    @Column(name = "addrbook_id", nullable = true, length = 11)
    private Long addrbookId;

    @Column(name = "token", nullable = true, length = 10)
    private String token;

    @Column(name = "expired_token", nullable = true)
    private Date expiredToken;

    @Column(name = "marriage_year", nullable = true, length = 10)
    private String marriageYear;

    @ManyToOne
    @JoinColumn(name="company_id",
            foreignKey = @ForeignKey(name = "fo_company_id"),
            insertable = false,
            updatable = false
    )
    public M_Company company;

    @Column(name = "company_id", nullable = false, length = 11)
    private Long companyId;

    @Column(name = "is_process", nullable = true)
    private boolean isProcess;

    @Column(name = "is_complete", nullable = true)
    private boolean isComplete;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPob() {
        return pob;
    }

    public void setPob(String pob) {
        this.pob = pob;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public Long getReligionId() {
        return religionId;
    }

    public void setReligionId(Long religionId) {
        this.religionId = religionId;
    }

    public Integer getHigh() {
        return high;
    }

    public void setHigh(Integer high) {
        this.high = high;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getEthnic() {
        return ethnic;
    }

    public void setEthnic(String ethnic) {
        this.ethnic = ethnic;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public Long getIdentityTypeId() {
        return identityTypeId;
    }

    public void setIdentityTypeId(Long identityTypeId) {
        this.identityTypeId = identityTypeId;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getPhoneNumber2() {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getParentPhoneNumber() {
        return parentPhoneNumber;
    }

    public void setParentPhoneNumber(String parentPhoneNumber) {
        this.parentPhoneNumber = parentPhoneNumber;
    }

    public String getChildSequence() {
        return childSequence;
    }

    public void setChildSequence(String childSequence) {
        this.childSequence = childSequence;
    }

    public String getHowManyBrothers() {
        return howManyBrothers;
    }

    public void setHowManyBrothers(String howManyBrothers) {
        this.howManyBrothers = howManyBrothers;
    }

    public Long getMaritalStatusId() {
        return maritalStatusId;
    }

    public void setMaritalStatusId(Long maritalStatusId) {
        this.maritalStatusId = maritalStatusId;
    }

    public Long getAddrbookId() {
        return addrbookId;
    }

    public void setAddrbookId(Long addrbookId) {
        this.addrbookId = addrbookId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiredToken() {
        return expiredToken;
    }

    public void setExpiredToken(Date expiredToken) {
        this.expiredToken = expiredToken;
    }

    public String getMarriageYear() {
        return marriageYear;
    }

    public void setMarriageYear(String marriageYear) {
        this.marriageYear = marriageYear;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public boolean isProcess() {
        return isProcess;
    }

    public void setProcess(boolean process) {
        isProcess = process;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
