package xa.miniproject.models;

import javax.persistence.*;

@Entity
@Table(name = "x_tro")
public class M_TRO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name="id", nullable=false)
    private Long id;

    @Column(name = "tro_name", nullable = false)
    private String troName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTroName() {
        return troName;
    }

    public void setTroName(String troName) {
        this.troName = troName;
    }
}
