package xa.miniproject.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "x_employee_leave")
public class M_EmployeeLeave {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on")
    private Date modifiedOn;

    @Column(name = "deleted_by", length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on")
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private Boolean isDelete;

    @Column(name = "employee_id", nullable = false, length = 11)
    private Long employeeId;

    @Column(name = "regular_quota", nullable = false)
    private Integer regularQuota;

    @Column(name = "annual_collective_leave", nullable = false)
    private Integer annualCollectiveLeave;

    @Column(name = "leave_already_taken", nullable = false)
    private Integer leaveAlreadyTaken;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getRegularQuota() {
        return regularQuota;
    }

    public void setRegularQuota(Integer regularQuota) {
        this.regularQuota = regularQuota;
    }

    public Integer getAnnualCollectiveLeave() {
        return annualCollectiveLeave;
    }

    public void setAnnualCollectiveLeave(Integer annualCollectiveLeave) {
        this.annualCollectiveLeave = annualCollectiveLeave;
    }

    public Integer getLeaveAlreadyTaken() {
        return leaveAlreadyTaken;
    }

    public void setLeaveAlreadyTaken(Integer leaveAlreadyTaken) {
        this.leaveAlreadyTaken = leaveAlreadyTaken;
    }
}
