package xa.miniproject.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "x_addrbook")
public class M_Addrbook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", nullable = true, length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on", nullable = true)
    private Date modifiedOn;

    @Column(name = "deleted_by", nullable = true, length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on", nullable = true)
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private boolean isDelete;

    @Column(name = "is_locked", nullable = false)
    public boolean isLocked;

    @Column(name = "attempt", nullable = false, length = 1)
    private int attempt;

    @Column(name = "email", nullable = false, length = 100)
    private String email;

    @Column(name = "abuid", nullable = false, length = 50)
    private String abuid;

    @Column(name = "abpwd", nullable = false, length = 50)
    private String abpwd;

    @Column(name = "fp_token", length = 100)
    private String fpToken;

    @Column(name = "fp_expired_date")
    private Date fpExpiredDate;

    @Column(name = "fp_counter", nullable = false, length = 3)
    private int fpCounter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public int getAttempt() {
        return attempt;
    }

    public void setAttempt(int attempt) {
        this.attempt = attempt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAbuid() {
        return abuid;
    }

    public void setAbuid(String abuid) {
        this.abuid = abuid;
    }

    public String getAbpwd() {
        return abpwd;
    }

    public void setAbpwd(String abpwd) {
        this.abpwd = abpwd;
    }

    public String getFpToken() {
        return fpToken;
    }

    public void setFpToken(String fpToken) {
        this.fpToken = fpToken;
    }

    public Date getFpExpiredDate() {
        return fpExpiredDate;
    }

    public void setFpExpiredDate(Date fpExpiredDate) {
        this.fpExpiredDate = fpExpiredDate;
    }

    public int getFpCounter() {
        return fpCounter;
    }

    public void setFpCounter(int fpCounter) {
        this.fpCounter = fpCounter;
    }
}
