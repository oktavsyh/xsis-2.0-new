package xa.miniproject.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "x_riwayat_pendidikan")
public class M_RiwayatPendidikan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on")
    private Date modifiedOn;

    @Column(name = "deleted_by", length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on")
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private Boolean isDelete;

    @Column(name = "biodata_id", nullable = false, length = 11)
    private Long biodataid;
    @ManyToOne
    @JoinColumn(
        name = "biodata_id",
        foreignKey = @ForeignKey(name ="fk_biodata_id"),
        insertable = false,
        updatable = false
    )
    public M_Biodata biodatapendidikan;

    @Column(name = "school_name", length = 100)
    private String schoolName;

    @Column(name = "city", length = 50)
    private String city;

    @Column(name = "country", length = 50)
    private String country;

    @Column(name = "education_level_id", length = 11)
    private Long  educationLevelid;

    @ManyToOne
    @JoinColumn(name = "education_level_id",
            foreignKey = @ForeignKey(name = "fe_education_level_id"),
            insertable = false,
            updatable = false)
    public M_EducationLevel edu;
    public M_RiwayatPendidikan(){

    }
    @Column(name = "entry_year", length = 10)
    private String entryYear;

    @Column(name = "graduation_year", length = 10)
    private String graduationYear;

    @Column(name = "major", length = 100)
    private String major;

    @Column(name = "gpa")
    private double gpa;

    @Column(name = "notes", length = 1000)
    private String notes;

    @Column(name = "orders")
    private Integer order;

    @Column(name = "judul_ta", length = 255)
    private String judulTa;

    @Column(name = "deskripsi_ta", length = 5000)
    private String deskripsiTa;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }

    public Long getBiodataid() {
        return biodataid;
    }

    public void setBiodataid(Long biodataid) {
        this.biodataid = biodataid;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getEducationLevelid() {
        return educationLevelid;
    }

    public void setEducationLevelid(Long educationLevelid) {
        this.educationLevelid = educationLevelid;
    }

    public String getEntryYear() {
        return entryYear;
    }

    public void setEntryYear(String entryYear) {
        this.entryYear = entryYear;
    }

    public String getGraduationYear() {
        return graduationYear;
    }

    public void setGraduationYear(String graduationYear) {
        this.graduationYear = graduationYear;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getorder() {
        return order;
    }

    public void setorder(Integer order) {
        order = order;
    }

    public String getJudulTa() {
        return judulTa;
    }

    public void setJudulTa(String judulTa) {
        this.judulTa = judulTa;
    }

    public String getDeskripsiTa() {
        return deskripsiTa;
    }

    public void setDeskripsiTa(String deskripsiTa) {
        this.deskripsiTa = deskripsiTa;
    }
}
