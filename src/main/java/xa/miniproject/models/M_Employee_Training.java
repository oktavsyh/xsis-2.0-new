package xa.miniproject.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "x_employee_training")
public class M_Employee_Training {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long Id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", nullable = true, length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on", nullable = true)
    private Date modifiedOn;

    @Column(name = "deleted_by", nullable = true, length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on", nullable = true)
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private Boolean isDelete;

    // Karyawan
    @Column(name = "employee_id", nullable = false, length = 11)
    private Long employeeId;

    @ManyToOne
    @JoinColumn(name = "employee_id", foreignKey = @ForeignKey(name = "fkey_employee_id"), insertable = false, updatable = false)
    public M_Employee m_Employee;

    // Pelatihan
    @Column(name = "training_id", nullable = false, length = 11)
    private Long trainingId;

    @ManyToOne
    @JoinColumn(name = "training_id", foreignKey = @ForeignKey(name = "fkey_training_id"), insertable = false, updatable = false)
    public M_Training m_Training;

    // Penyelenggara
    @Column(name = "training_organizer_id", nullable = true, length = 11)
    private Long trainingOrganizerId;

    @ManyToOne
    @JoinColumn(name = "training_organizer_id", foreignKey = @ForeignKey(name = "fkey_training_organizer_id"), insertable = false, updatable = false)
    public M_Training_Organizer m_Training_Organizer;

    @Column(name = "training_date", nullable = true)
    private Date trainingDate;

    // Jenis Pelatihan
    @Column(name = "training_type_id", nullable = true, length = 11)
    private Long trainingTypeId;

    @ManyToOne
    @JoinColumn(name = "training_type_id", foreignKey = @ForeignKey(name = "fkey_training_type_id"), insertable = false, updatable = false)
    public M_Training_Type m_Training_Type;

    // Jenis Sertifikasi
    @Column(name = "certification_type_id", nullable = true, length = 11)
    private Long certificationTypeId;

    @ManyToOne
    @JoinColumn(name = "certification_type_id", foreignKey = @ForeignKey(name = "fkey_certification_type_id"), insertable = false, updatable = false)
    public M_Certification_Type m_Certification_Type;

    @Column(name = "status", nullable = false, length = 15)
    private String status;

    public M_Employee_Training() {

    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return this.deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return this.deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Boolean isIsDelete() {
        return this.isDelete;
    }

    public Boolean getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Long getEmployeeId() {
        return this.employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public M_Employee getM_Employee() {
        return this.m_Employee;
    }

    public void setM_Employee(M_Employee m_Employee) {
        this.m_Employee = m_Employee;
    }

    public Long getTrainingId() {
        return this.trainingId;
    }

    public void setTrainingId(Long trainingId) {
        this.trainingId = trainingId;
    }

    public M_Training getM_Training() {
        return this.m_Training;
    }

    public void setM_Training(M_Training m_Training) {
        this.m_Training = m_Training;
    }

    public Long getTrainingOrganizerId() {
        return this.trainingOrganizerId;
    }

    public void setTrainingOrganizerId(Long trainingOrganizerId) {
        this.trainingOrganizerId = trainingOrganizerId;
    }

    public M_Training_Organizer getM_Training_Organizer() {
        return this.m_Training_Organizer;
    }

    public void setM_Training_Organizer(M_Training_Organizer m_Training_Organizer) {
        this.m_Training_Organizer = m_Training_Organizer;
    }

    public Date getTrainingDate() {
        return this.trainingDate;
    }

    public void setTrainingDate(Date trainingDate) {
        this.trainingDate = trainingDate;
    }

    public Long getTrainingTypeId() {
        return this.trainingTypeId;
    }

    public void setTrainingTypeId(Long trainingTypeId) {
        this.trainingTypeId = trainingTypeId;
    }

    public M_Training_Type getM_Training_Type() {
        return this.m_Training_Type;
    }

    public void setM_Training_Type(M_Training_Type m_Training_Type) {
        this.m_Training_Type = m_Training_Type;
    }

    public Long getCertificationTypeId() {
        return this.certificationTypeId;
    }

    public void setCertificationTypeId(Long certificationTypeId) {
        this.certificationTypeId = certificationTypeId;
    }

    public M_Certification_Type getM_Certification_Type() {
        return this.m_Certification_Type;
    }

    public void setM_Certification_Type(M_Certification_Type m_Certification_Type) {
        this.m_Certification_Type = m_Certification_Type;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}