package xa.miniproject.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_sertifikasi")
public class M_Sertifikasi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false)
    private Long Id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", nullable = true, length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on", nullable = true)
    private Date modifiedOn;

    @Column(name = "deleted_by", nullable = true)
    private Long deletedBy;

    @Column(name = "deleted_on", nullable = true)
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private Boolean isDelete;

    @Column(name = "biodata_id", nullable = false, length = 11)
    private Long biodataId;

    @Column(name = "certificate_name", nullable = true, length = 200)
    private String certificateName;

    @Column(name = "publisher", nullable = true, length = 100)
    private String publisher;

    @Column(name = "valid_start_year", nullable = true, length = 10)
    private String validStartYear;

    @Column(name = "valid_start_month", nullable = true, length = 10)
    private String validStartMonth;

    @Column(name = "until_year", nullable = true, length = 10)
    private String untilYear;

    @Column(name = "until_month", nullable = true, length = 10)
    private String untilMonth;

    @Column(name = "notes", nullable = true, length = 1000)
    private String notes;


    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return this.deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return this.deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Boolean isIsDelete() {
        return this.isDelete;
    }

    public Boolean getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Long getBiodataId() {
        return this.biodataId;
    }

    public void setBiodataId(Long biodataId) {
        this.biodataId = biodataId;
    }

    public String getCertificateName() {
        return this.certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getValidStartYear() {
        return this.validStartYear;
    }

    public void setValidStartYear(String validStartYear) {
        this.validStartYear = validStartYear;
    }

    public String getValidStartMonth() {
        return this.validStartMonth;
    }

    public void setValidStartMonth(String validStartMonth) {
        this.validStartMonth = validStartMonth;
    }

    public String getUntilYear() {
        return this.untilYear;
    }

    public void setUntilYear(String untilYear) {
        this.untilYear = untilYear;
    }

    public String getUntilMonth() {
        return this.untilMonth;
    }

    public void setUntilMonth(String untilMonth) {
        this.untilMonth = untilMonth;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


}