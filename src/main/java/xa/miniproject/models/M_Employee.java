package xa.miniproject.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "x_employee")
public class M_Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long Id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", nullable = true, length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on", nullable = true)
    private Date modifiedOn;

    @Column(name = "deleted_by", nullable = true, length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on", nullable = true)
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private Boolean isDelete;

    // Join Biodata
    @Column(name = "biodata_id", nullable = false, length = 11)
    private Long biodataId;

    @ManyToOne
    @JoinColumn(name = "biodata_id", foreignKey = @ForeignKey(name = "fkey_biodata_id"), insertable = false, updatable = false)
    public M_Biodata m_Biodata;

    @Column(name = "is_idle", nullable = true)
    private Boolean isIdle;

    @Column(name = "is_ero", nullable = true)
    private Boolean isEro;

    @Column(name = "is_user_client", nullable = true)
    private Boolean isUserClient;

    @Column(name = "ero_email", nullable = true, length = 100)
    private String eroEmail;

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return this.deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return this.deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Boolean isIsDelete() {
        return this.isDelete;
    }

    public Boolean getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Long getBiodataId() {
        return this.biodataId;
    }

    public void setBiodataId(Long biodataId) {
        this.biodataId = biodataId;
    }

    public Boolean isIsIdle() {
        return this.isIdle;
    }

    public Boolean getIsIdle() {
        return this.isIdle;
    }

    public void setIsIdle(Boolean isIdle) {
        this.isIdle = isIdle;
    }

    public Boolean isIsEro() {
        return this.isEro;
    }

    public Boolean getIsEro() {
        return this.isEro;
    }

    public void setIsEro(Boolean isEro) {
        this.isEro = isEro;
    }

    public Boolean isIsUserClient() {
        return this.isUserClient;
    }

    public Boolean getIsUserClient() {
        return this.isUserClient;
    }

    public void setIsUserClient(Boolean isUserClient) {
        this.isUserClient = isUserClient;
    }

    public String getEroEmail() {
        return this.eroEmail;
    }

    public void setEroEmail(String eroEmail) {
        this.eroEmail = eroEmail;
    }

}