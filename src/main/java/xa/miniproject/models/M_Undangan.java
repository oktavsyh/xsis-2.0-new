package xa.miniproject.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "x_undangan")
public class M_Undangan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long Id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on")
    private Date modifiedOn;

    @Column(name = "deleted_by", length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on")
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private Boolean isDelete;

    @Column(name = "schedule_type_id", length = 11)
    public Long scheduleTypeId;

    @ManyToOne
    @JoinColumn(
            name = "schedule_type_id",
            foreignKey = @ForeignKey(name ="fk_st_id_undangan"),
            insertable = false,
            updatable = false
    )

    public M_Schedule_Type m_schedule_type;

    public M_Undangan() {

    }

    @Column(name = "invitation_date")
    private Date invitationDate;

    @Column(name = "invitation_code", length = 20)
    private String invitationCode;

    @Column(name = "time", length = 10)
    private String time;

    @Column(name = "ro", length = 11)
    private Long ro;

    @ManyToOne
    @JoinColumn(
            name = "ro",
            foreignKey = @ForeignKey(name ="fk_ro_id_undangan"),
            insertable = false,
            updatable = false
    )

    public M_RO m_ro;

    @Column(name = "tro", length = 11)
    private Long tro;

    @ManyToOne
    @JoinColumn(
            name = "tro",
            foreignKey = @ForeignKey(name ="fk_tro_id_undangan"),
            insertable = false,
            updatable = false
    )

    public M_TRO m_tro;

    @Column(name = "other_ro_tro", length = 100)
    private String otherRoTro;

    @Column(name = "location", length = 100)
    private String location;

    @Column(name = "status", length = 50)
    private String status;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }

    public Long getScheduleTypeId() {
        return scheduleTypeId;
    }

    public void setScheduleTypeId(Long scheduleTypeId) {
        this.scheduleTypeId = scheduleTypeId;
    }

    public Date getInvitationDate() {
        return invitationDate;
    }

    public void setInvitationDate(Date invitationDate) {
        this.invitationDate = invitationDate;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Long getRo() {
        return ro;
    }

    public void setRo(Long ro) {
        this.ro = ro;
    }

    public Long getTro() {
        return tro;
    }

    public void setTro(Long tro) {
        this.tro = tro;
    }

    public String getOtherRoTro() {
        return otherRoTro;
    }

    public void setOtherRoTro(String otherRoTro) {
        this.otherRoTro = otherRoTro;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public M_Schedule_Type getM_schedule_type() {
        return m_schedule_type;
    }

    public void setM_schedule_type(M_Schedule_Type m_schedule_type) {
        this.m_schedule_type = m_schedule_type;
    }

    public M_RO getM_ro() {
        return m_ro;
    }

    public void setM_ro(M_RO m_ro) {
        this.m_ro = m_ro;
    }

    public M_TRO getM_tro() {
        return m_tro;
    }

    public void setM_tro(M_TRO m_tro) {
        this.m_tro = m_tro;
    }
}