package xa.miniproject.models;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "x_rencana_jadwal")
public class M_Rencana_Jadwal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id", nullable = false, length = 11)
    private Long id;

    @Column(name = "created_by", nullable = false, length = 11)
    private Long createdBy;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "modified_by", nullable = true, length = 11)
    private Long modifiedBy;

    @Column(name = "modified_on", nullable = true)
    private Date modifiedOn;

    @Column(name = "deleted_by", nullable = true, length = 11)
    private Long deletedBy;

    @Column(name = "deleted_on", nullable = true)
    private Date deletedOn;

    @Column(name = "is_delete", nullable = false)
    private boolean isDelete;

    @Column(name = "schedule_code", nullable = true, length = 20)
    private String scheduleCode;

    @Column(name = "schedule_date", nullable = true, length = 11)
    private Date scheduleDate;

    @Column(name = "time", nullable = true, length = 10)
    private String time;

    @Column(name = "ro", nullable = true, length = 11)
    private Long ro;
    @ManyToOne
    @JoinColumn(
            name = "ro",
            foreignKey = @ForeignKey(name = "fk_ro"),
            insertable = false,
            updatable = false
    )
    public M_RO modelro;

    @Column(name = "tro", nullable = true, length = 11)
    private Long tro;
    @ManyToOne
    @JoinColumn(
            name = "tro",
            foreignKey = @ForeignKey(name = "fk_tro"),
            insertable = false,
            updatable = false
    )
    public M_TRO modeltro;

    @Column(name = "schedule_type_id", nullable = true, length = 11)
    private Long scheduleTypeId;
    @ManyToOne
    @JoinColumn(
            name = "schedule_type_id",
            foreignKey = @ForeignKey(name = "fk_schedule_type_id"),
            insertable = false,
            updatable = false
    )
    public M_Schedule_Type schedule;

    public M_Rencana_Jadwal(){

    }
    @Column(name = "location", nullable = true, length = 100)
    private String location;

    @Column(name = "other_ro_tro", nullable = true, length = 100)
    private String otherRoTro;

    @Column(name = "notes", nullable = true, length = 1000)
    private String notes;

    @Column(name = "is_automatic_mail", nullable = false)
    public Boolean isAutomaticMail;

    @Column(name = "sent_date", nullable = true)
    private Date sentDate;

    @Column(name = "status", nullable = true, length = 50)
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public String getScheduleCode() {
        return scheduleCode;
    }

    public void setScheduleCode(String scheduleCode) {
        this.scheduleCode = scheduleCode;
    }

    public Date getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(Date scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Long getRo() {
        return ro;
    }

    public void setRo(Long ro) {
        this.ro = ro;
    }

    public Long getTro() {
        return tro;
    }

    public void setTro(Long tro) {
        this.tro = tro;
    }

    public Long getScheduleTypeId() {
        return scheduleTypeId;
    }

    public void setScheduleTypeId(Long scheduleTypeId) {
        this.scheduleTypeId = scheduleTypeId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOtherRoTro() {
        return otherRoTro;
    }

    public void setOtherRoTro(String otherRoTro) {
        this.otherRoTro = otherRoTro;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Boolean getIsAutomaticMail() {
        return isAutomaticMail;
    }

    public void setIsAutomaticMail(Boolean isAutomaticMail) {
        this.isAutomaticMail = isAutomaticMail;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
