package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Organisasi;

import java.util.List;

public interface R_Organisasi extends JpaRepository<M_Organisasi, Long> {
    @Query(value = "SELECT * FROM x_organisasi WHERE biodata_id= ?1 and is_delete=false ORDER BY id ", nativeQuery = true)
    public List<M_Organisasi> findByBioId(Long id);
}
