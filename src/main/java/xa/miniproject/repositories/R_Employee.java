package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import xa.miniproject.models.M_Employee;

/**
 * R_Employee
 */
public interface R_Employee extends JpaRepository<M_Employee, Long> {

}