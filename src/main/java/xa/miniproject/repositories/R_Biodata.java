package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Biodata;

public interface R_Biodata extends JpaRepository<M_Biodata, Long> {
    @Query(value = "SELECT id from x_biodata where phone_number1 = ?1 or phone_number2 = ?2 or email = ?3 or identity_no = ?4 or parent_phone_number = ?5", nativeQuery = true)
    Long validate100(String ph1, String ph2, String ph3, String em, String idno);

    @Query(value = "SELECT id from x_biodata where (identity_no = ?1) and (identity_type_id = ?2) limit 1", nativeQuery = true)
    Long validate0(String ph1, Long ph2);

    @Query(value = "SELECT id from x_biodata where email=?1 ", nativeQuery = true)
    Long validate(String ph1);

    @Query(value = "SELECT id from x_biodata where phone_number1 = ?1 limit 1", nativeQuery = true)
    Long validate1(String ph1);

    @Query(value = "SELECT id from x_biodata where phone_number2 = ?1 limit 1", nativeQuery = true)
    Long validate2(String ph2);

    @Query(value = "SELECT id from x_biodata where parent_phone_number = ?1 limit 1", nativeQuery = true)
    Long validate3(String ph3);

    @Query(value = "SELECT id FROM x_biodata order by id desc limit 1", nativeQuery = true)
    public Long findBymaxid();

    @Query(value = "SELECT * FROM x_biodata WHERE id= ?1 ORDER BY id", nativeQuery = true)
    public M_Biodata findByBioId(Long id);

    @Query(value = "SELECT * FROM x_biodata WHERE fullname= ?1 ORDER BY id", nativeQuery = true)
    public M_Biodata findByFullName(String name);

    @Query(value = "SELECT * from x_biodata where fullname=?1 ", nativeQuery = true)
    M_Biodata getinfo(String ph1);

}
