package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_RO;

public interface R_RO extends JpaRepository<M_RO, Long> {
}
