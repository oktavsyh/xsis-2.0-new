package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import xa.miniproject.models.M_Training_Organizer;

/**
 * R_Training_Organizer
 */
public interface R_Training_Organizer extends JpaRepository<M_Training_Organizer, Long> {

}