package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Rencana_Jadwal;
import xa.miniproject.models.M_Rencana_Jadwal_Detail;

import java.util.List;

public interface R_Rencana_Jadwal_Detail extends JpaRepository<M_Rencana_Jadwal_Detail, Long> {
    @Query(value = "SELECT * FROM x_rencana_jadwal_detail WHERE rencana_jadwal_id= ?1 ORDER BY id",nativeQuery = true)
    public List<M_Rencana_Jadwal_Detail> findbyrencid(Long id);
    @Query(value = "SELECT * FROM x_rencana_jadwal_detail WHERE is_delete = false AND biodata_id = ?1", nativeQuery = true)
    public List<M_Rencana_Jadwal_Detail> findJadByBio(Long id);
    @Query(value = "select count(*) from x_rencana_jadwal_detail where rencana_jadwal_id = ?1", nativeQuery = true)
    public Long findjumlahdata(Long id);
}
