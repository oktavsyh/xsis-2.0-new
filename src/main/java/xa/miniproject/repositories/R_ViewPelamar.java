package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_ViewPelamar;

import java.util.List;

public interface R_ViewPelamar extends JpaRepository<M_ViewPelamar, Long> {
    @Query(value = "SELECT * FROM view_pelamar WHERE is_delete = false", nativeQuery = true)
    public List<M_ViewPelamar> findproId();
    @Query(value = "SELECT * FROM view_pelamar WHERE is_delete = false", nativeQuery = true)
    public List<M_ViewPelamar> findpelamar();
    @Query(value = "SELECT * FROM view_pelamar WHERE is_delete = false AND fullname LIKE %?1%", nativeQuery = true)
    public List<M_ViewPelamar> findpelamarname(String a);
}
