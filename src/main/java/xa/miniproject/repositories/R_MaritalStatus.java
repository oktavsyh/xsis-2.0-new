
package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_MaritalStatus;
import xa.miniproject.models.M_Religion;

public interface R_MaritalStatus extends JpaRepository<M_MaritalStatus,Long> {
}