package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_ViewProsesPelamar;

import java.util.List;

public interface R_ViewProsesPelamar extends JpaRepository<M_ViewProsesPelamar, Long> {
    @Query(value = "SELECT * FROM view_prosespelamar WHERE is_delete = false AND is_complete = false AND is_process = true", nativeQuery = true)
    public List<M_ViewProsesPelamar> findprosestrue();
    @Query(value = "SELECT * FROM view_prosespelamar WHERE is_delete = false AND is_complete = false AND is_process = false", nativeQuery = true)
    public List<M_ViewProsesPelamar> findprosesfalse();
}
