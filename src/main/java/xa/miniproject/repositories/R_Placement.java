package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import xa.miniproject.models.M_Placement;

import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Placement;

import java.util.List;

public interface R_Placement extends JpaRepository<M_Placement, Long> {
    @Query(value = "SELECT * FROM x_placement WHERE is_placement_active = true ORDER BY id ", nativeQuery = true)
    public List<M_Placement> findByBioId();
}
