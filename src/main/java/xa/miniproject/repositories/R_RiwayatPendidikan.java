package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Biodata;
import xa.miniproject.models.M_RiwayatPendidikan;

import java.util.List;

public interface R_RiwayatPendidikan extends JpaRepository<M_RiwayatPendidikan, Long> {
    @Query(value = "SELECT * FROM x_riwayat_pendidikan WHERE is_delete = false AND biodata_id= ?1 ORDER BY id ", nativeQuery = true)
    public List<M_RiwayatPendidikan> findByBioId(Long id);

    @Query(value = "SELECT school_name FROM x_riwayat_pendidikan WHERE graduation_year in (select max(graduation_year) from x_riwayat_pendidikan where biodata_id= ?1)", nativeQuery = true)
    public List<M_RiwayatPendidikan> findnamauniv(Long id);

    @Query(value = "SELECT major FROM x_riwayat_pendidikan WHERE graduation_year in (select max(graduation_year) from x_riwayat_pendidikan where biodata_id= ?1)", nativeQuery = true)
    public List<M_RiwayatPendidikan> findmajor(Long id);

    @Query(value = "select b.id, b.fullname, b.nick_name, b.email, b.phone_number1, (SELECT school_name FROM x_riwayat_pendidikan WHERE graduation_year = (select max(graduation_year) from x_riwayat_pendidikan where biodata_id=b.id) and biodata_id=b.id  limit 1) as school_name, (SELECT major FROM x_riwayat_pendidikan WHERE graduation_year = (select max(graduation_year) from x_riwayat_pendidikan where biodata_id=b.id) and biodata_id=b.id  limit 1) as major from x_biodata b", nativeQuery = true)
    public List<String> findnamapendidikan();

    @Query(value = "SELECT * FROM x_riwayat_pendidikan WHERE biodata_id= ?1 ORDER BY id ", nativeQuery = true)
    public M_RiwayatPendidikan findByBiolagiId(Long id);
}
