package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_EmployeeLeave;

public interface R_EmployeeLeave extends JpaRepository<M_EmployeeLeave, Long> {
}
