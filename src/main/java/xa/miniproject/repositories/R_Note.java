package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_Note;

public interface R_Note extends JpaRepository<M_Note, Long> {
}
