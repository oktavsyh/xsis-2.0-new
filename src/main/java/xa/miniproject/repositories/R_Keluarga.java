package xa.miniproject.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import xa.miniproject.models.M_Keluarga;

public interface R_Keluarga extends JpaRepository<M_Keluarga, Long> {
  @Query(value = "SELECT * FROM x_keluarga WHERE biodata_id= ?1 and is_delete= ?2 ORDER BY id", nativeQuery = true)
  public List<M_Keluarga> findByBioId(Long id, Boolean id2);
}
