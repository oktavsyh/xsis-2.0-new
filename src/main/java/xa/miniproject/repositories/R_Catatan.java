package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Catatan;

import java.util.List;

public interface R_Catatan extends JpaRepository<M_Catatan, Long> {
    @Query(value = "SELECT * FROM x_catatan WHERE biodata_id= ?1 and is_delete=false ORDER BY id ", nativeQuery = true)
    public List<M_Catatan> findByBioId(Long id);
}
