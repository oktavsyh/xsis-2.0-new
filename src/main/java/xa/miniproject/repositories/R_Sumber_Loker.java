package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Sumber_Loker;

import java.util.List;

public interface R_Sumber_Loker extends JpaRepository <M_Sumber_Loker, Long> {
    @Query(value = "SELECT * FROM x_sumber_loker WHERE is_delete = false AND biodata_id= ?1 ORDER BY id ", nativeQuery = true)
    public M_Sumber_Loker findByBioId(Long id);
}
