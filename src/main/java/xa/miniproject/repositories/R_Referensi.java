package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Referensi;

import java.util.List;

public interface R_Referensi extends JpaRepository<M_Referensi, Long> {
    @Query(value = "SELECT * FROM x_pe_referensi WHERE biodata_id= ?1 AND is_delete=false", nativeQuery = true)
    public List<M_Referensi> findByBioId(Long id);
}
