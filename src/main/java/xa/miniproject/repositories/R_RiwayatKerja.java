package xa.miniproject.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import xa.miniproject.models.M_RiwayatKerja;


public interface R_RiwayatKerja extends JpaRepository<M_RiwayatKerja, Long> {
    @Query(value = "SELECT * FROM x_riwayat_pekerjaan WHERE is_delete = false AND biodata_id= ?1", nativeQuery = true)
    public List <M_RiwayatKerja> findByBioId(Long id);
}
