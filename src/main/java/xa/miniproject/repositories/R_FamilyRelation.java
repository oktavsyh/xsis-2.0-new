package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Family_Relation;

public interface R_FamilyRelation extends JpaRepository<M_Family_Relation, Long> {
    @Query(value = "SELECT * FROM x_family_relation WHERE family_tree_type_id = ?1", nativeQuery = true)
    public List<M_Family_Relation> cari_hubungan(Long id);

}