package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import xa.miniproject.models.M_Role;

import java.util.List;

public interface R_Role extends JpaRepository<M_Role, Long> {
    @Query(value = "SELECT id from x_role where code = ?1", nativeQuery = true)
    Long validate (String ph1);
    @Query(value = "SELECT * FROM x_role WHERE is_delete= false ORDER BY id", nativeQuery = true)
    public List<M_Role> findByBioId();
}
