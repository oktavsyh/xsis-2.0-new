package xa.miniproject.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import xa.miniproject.models.M_EducationLevel;
import xa.miniproject.models.M_Family_Tree_Type;

public interface R_EducationalLevel extends JpaRepository<M_EducationLevel, Long> {
    @Query(value = "SELECT * FROM x_family_tree_type ORDER BY id", nativeQuery = true)
    List<M_EducationLevel> viewlisteducationlevel();
}
