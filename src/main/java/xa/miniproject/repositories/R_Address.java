package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Address;

import java.util.List;

public interface R_Address extends JpaRepository<M_Address, Long> {
    @Query(value = "SELECT * FROM x_address WHERE biodata_id = ?1", nativeQuery = true)
    public List<M_Address> findAddByBioId(Long id);
}
