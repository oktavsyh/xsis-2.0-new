package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Undangan;

import java.util.List;

public interface R_Undangan extends JpaRepository<M_Undangan, Long> {
    @Query(value = "SELECT id FROM x_undangan ORDER BY id DESC LIMIT 1", nativeQuery = true)
    public Long findLastId();
}
