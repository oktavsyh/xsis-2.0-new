package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_Addrbook;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface R_Addrbook extends JpaRepository<M_Addrbook, Long> {
    @Query(value = "SELECT COUNT(id) FROM x_addrbook WHERE is_delete = false AND email=?1 ", nativeQuery = true)
    Long validateemail(String ph1);

    @Query(value = "SELECT * FROM x_addrbook WHERE is_delete = false AND email=?1 ", nativeQuery = true)
    public M_Addrbook getdataAB(String ph1);

    @Query(value = "SELECT id FROM x_addrbook WHERE is_delete = false ORDER BY id DESC limit 1", nativeQuery=true)
    public Long findmaxid();

    @Query(value = "SELECT * FROM x_addrbook WHERE is_delete = false ", nativeQuery = true)
    public List<M_Addrbook> findAllfalse();

    @Query(value = "SELECT * FROM x_addrbook WHERE is_delete = false AND id = ?1", nativeQuery = true)
    public M_Addrbook findfalsebyid(Long id);
}
