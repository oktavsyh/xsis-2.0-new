package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

import xa.miniproject.models.M_Sertifikasi;

public interface R_Sertifikasi extends JpaRepository<M_Sertifikasi, Long> {
    @Query(value = "SELECT * FROM x_sertifikasi WHERE biodata_id= ?1 and is_delete= ?2 ORDER BY id", nativeQuery = true)
    public List<M_Sertifikasi> findByBioId(Long id, Boolean id2);
}