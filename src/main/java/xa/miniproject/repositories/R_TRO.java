package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_TRO;

public interface R_TRO extends JpaRepository<M_TRO, Long> {
}
