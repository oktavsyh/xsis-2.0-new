package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import xa.miniproject.models.M_Training_Type;

/**
 * R_Training_Type
 */
public interface R_Training_Type extends JpaRepository<M_Training_Type, Long> {

}