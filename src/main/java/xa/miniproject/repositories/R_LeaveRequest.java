package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_LeaveRequest;

public interface R_LeaveRequest extends JpaRepository<M_LeaveRequest, Long> {
}
