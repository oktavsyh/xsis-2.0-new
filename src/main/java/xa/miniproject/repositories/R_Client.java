package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;


import xa.miniproject.models.M_Client;

/**
 * R_Client
 */
public interface R_Client extends JpaRepository <M_Client,Long>{

    
}

