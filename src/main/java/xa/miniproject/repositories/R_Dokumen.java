package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Dokumen;

import java.util.List;

public interface R_Dokumen extends JpaRepository<M_Dokumen, Long> {
    @Query(value = "SELECT * FROM x_biodata_attachment WHERE biodata_id= ?1 AND is_delete=false", nativeQuery = true)
    public List<M_Dokumen> findByBioId(Long id);
    @Query(value ="select * from x_biodata_attachment where id in(select max(id) from x_biodata_attachment where biodata_id=?1) and biodata_id=?1 and is_photo=true and is_delete=false order by id", nativeQuery = true)
    public M_Dokumen fotoprofil(Long id);
}
