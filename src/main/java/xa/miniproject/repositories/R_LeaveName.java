package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_LeaveName;

public interface R_LeaveName extends JpaRepository<M_LeaveName, Long> {
}
