package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.*;

import xa.miniproject.models.M_Training;

public interface R_Training extends JpaRepository<M_Training, Long> {
    @Query(value = "SELECT * FROM x_training WHERE id = ?1", nativeQuery = true)
    public List<M_Training> cari_training(Long id);

}