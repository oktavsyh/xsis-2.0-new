package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Family_Tree_Type;
import java.util.*;

public interface R_FamilyTreeType extends JpaRepository<M_Family_Tree_Type, Long> {
    @Query(value = "SELECT * FROM x_family_tree_type ORDER BY id", nativeQuery = true)
    List<M_Family_Tree_Type> viewListMFamilyTreeType();
}