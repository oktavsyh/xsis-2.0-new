package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_Company;

public interface R_Company extends JpaRepository<M_Company, Long> {
}
