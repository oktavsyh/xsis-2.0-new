package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_SkillLevel;

public interface R_SkillLevel extends JpaRepository<M_SkillLevel, Long> {
}
