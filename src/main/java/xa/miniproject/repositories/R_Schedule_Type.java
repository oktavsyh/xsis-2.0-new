package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_Schedule_Type;

public interface R_Schedule_Type extends JpaRepository<M_Schedule_Type, Long> {
}
