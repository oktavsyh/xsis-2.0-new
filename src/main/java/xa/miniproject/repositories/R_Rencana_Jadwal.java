package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Rencana_Jadwal;

import java.util.Date;
import java.util.List;

public interface R_Rencana_Jadwal extends JpaRepository<M_Rencana_Jadwal, Long> {
    @Query(value = "SELECT * FROM x_rencana_jadwal WHERE schedule_date >= ?1 AND schedule_date <= ?2", nativeQuery = true)
    public List<M_Rencana_Jadwal> findByDate(Date date1, Date date2);
    @Query(value = "SELECT id FROM x_rencana_jadwal order by id desc limit 1", nativeQuery=true)
    public Long findBymaxid();
}
