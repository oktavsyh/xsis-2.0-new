package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Timesheet;

import java.util.Date;
import java.util.List;

public interface R_Timesheet extends JpaRepository<M_Timesheet, Long> {
    @Query(value = "SELECT * FROM x_timesheet WHERE timesheet_date >= ?1 AND timesheet_date <= ?2 AND is_delete= false ORDER BY id", nativeQuery = true)
    public List<M_Timesheet> findByDate(Date date1, Date date2);
    @Query(value = "SELECT * FROM x_timesheet WHERE is_delete= false ORDER BY id", nativeQuery = true)
    public List<M_Timesheet> findByBioId();
    @Query(value = "select*from x_timesheet  where timesheet_date between ?1 and ?2 AND is_delete= false ORDER BY id", nativeQuery = true)
    public List<M_Timesheet> findByBetween(Date date1, Date date2);
    @Query(value = "SELECT * from x_timesheet where id = ?1", nativeQuery = true)
    public List <M_Timesheet> validate(Long id);
    @Query(value = "SELECT id from x_timesheet where timesheet_date = ?1", nativeQuery = true)
    Long validateDate (Date date1);


}
