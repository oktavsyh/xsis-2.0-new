package xa.miniproject.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Keahlian;

import java.util.List;

public interface R_Keahlian extends JpaRepository<M_Keahlian, Long> {
    @Query(value = "SELECT * FROM x_keahlian WHERE biodata_id= ?1 AND is_delete= false", nativeQuery = true)
    public List<M_Keahlian> findByBioId(Long id);
}
