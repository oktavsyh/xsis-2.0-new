package xa.miniproject.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import xa.miniproject.models.M_Employee_Training;

/**
 * R_Employee_Training
 */
public interface R_Employee_Training extends JpaRepository<M_Employee_Training, Long> {
    @Query(value = "SELECT * FROM x_employee_training WHERE is_delete = false", nativeQuery = true)
    public List<M_Employee_Training> findemployee1();

    @Query(value = "SELECT * FROM x_employee_training JOIN x_employee ON x_employee.id = x_employee_training.employee_id JOIN x_biodata ON x_employee.biodata_id = x_biodata.id JOIN x_training ON x_training.id = x_employee_training.training_id WHERE x_biodata.fullname LIKE ?1% AND x_training.id = ?2 AND x_employee_training.is_delete = false", nativeQuery = true)
    public List<M_Employee_Training> find_name_training(String id1, Long id2);

    @Query(value = "SELECT * FROM x_employee_training WHERE is_delete = false AND biodata_id= ?1", nativeQuery = true)
    public List<M_Employee_Training> findemployee2(Long id);

    @Query(value = "SELECT * from x_employee_training WHERE employee_id = ?1", nativeQuery = true)
    public M_Employee_Training validateFullName(Long v2);
}