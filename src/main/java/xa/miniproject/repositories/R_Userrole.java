package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Userrole;

import java.util.List;

public interface R_Userrole extends JpaRepository<M_Userrole, Long> {
    @Query(value = "SELECT * FROM x_userrole WHERE is_delete = false AND addrbook_id=?1 ", nativeQuery = true)
    public List<M_Userrole> finddataUR(Long id);
}
