package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_UndanganDetail;

import java.util.List;

public interface R_UndanganDetail extends JpaRepository<M_UndanganDetail, Long> {
    @Query(value = "SELECT undangan_id FROM x_undangan_detail ORDER BY undangan_id DESC LIMIT 1", nativeQuery = true)
    public M_UndanganDetail findLastId();
    @Query(value = "SELECT * FROM x_undangan_detail WHERE is_delete = false", nativeQuery = true)
    public List<M_UndanganDetail> finduddId();
    @Query(value = "SELECT * FROM x_undangan_detail WHERE is_delete = false AND biodata_id = ?1", nativeQuery = true)
    public List<M_UndanganDetail> findUndByBio(Long id);
    @Query(value = "select count(*) from x_undangan_detail where undangan_id = ?1", nativeQuery = true)
    public Long findjumlahdata(Long id);
}
