package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.miniproject.models.M_Keterangan_Tambahan;

import java.util.List;

public interface R_Keterangan_Tambahan extends JpaRepository<M_Keterangan_Tambahan, Long> {
    @Query(value = "SELECT * FROM x_keterangan_tambahan WHERE biodata_id= ?1 AND is_delete = false", nativeQuery = true)
    public List<M_Keterangan_Tambahan> findByBioId(Long id);
}
