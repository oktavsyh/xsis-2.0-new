package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import xa.miniproject.models.M_Certification_Type;

/**
 * R_Certification_Type
 */
public interface R_Certification_Type extends JpaRepository<M_Certification_Type, Long> {

}