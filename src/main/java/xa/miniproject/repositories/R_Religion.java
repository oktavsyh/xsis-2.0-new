package xa.miniproject.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import xa.miniproject.models.M_Religion;

/**
 * R_Religion
 */
public interface R_Religion extends JpaRepository<M_Religion, Long> {
    @Query(value = "select * from x_religion where is_delete=false", nativeQuery = true)

    List<M_Religion> showreligion();

}