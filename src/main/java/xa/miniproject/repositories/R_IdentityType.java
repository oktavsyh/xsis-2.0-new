package xa.miniproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.miniproject.models.M_IdentityType;
import xa.miniproject.models.M_Religion;

public interface R_IdentityType extends JpaRepository<M_IdentityType,Long> {
}
