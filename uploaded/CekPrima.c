#include <stdio.h>
int cekPrima(int);

void main()
{
    int bil, hasil;

    printf("Masukkan bilangan : ");
    scanf("%d",&bil);

    hasil = cekPrima(bil);
    if(hasil == 1){
        printf("Bilangan %d adalah bilangan prima",bil);
    }else{
        printf("Bilangan %d bukan bilangan prima",bil);
    }
}

int cekPrima(int cek)
{
    int x,y = 0,ret;

    for(x=1;x<=cek;x++){
        if(cek % x == 0){
            y++;
        }
    }
    if(y==2){
        return 1;
    }else{
        return 0;
    }
}
