$(document).ready(function(){
            console.log("Document is ready");
            loadData();
            $('#savebutton').click(function(){
                console.log("Button save diklik ...");
                savedata();
            })
        })

        function editform(id, nameval) {
            console.log("Di fungsi edit form: " + id);
            $("#nameedit").val(nameval);
        }

        function saveedit(id) {
            $.ajax({
                url: '/api/category/' + id,
                type: 'post',
                data: '{ "name" : "' + $('#nameedit').val() +  '" }',
                success: function() {
                    loadData();
                },
                dataType: 'json',
                contentType: 'application/json'
            })
        }

        function savedata() {
            $.ajax({
                url: '/api/category/',
                type: 'post',
                data: '{ "name" : "' + $('#name').val() +  '" }',
                success: function() {
                    loadData();
                },
                dataType: 'json',
                contentType: 'application/json'
            })
        }

        function loadData() {
            $.ajax ({
                url: '/api/category/',
                type: 'get',
                success: function(result) {
                    console.log(result);
                    var str = "<table class = 'table table-striped'>";
                    for(i=0 ; i<result.length ; i++) {
                        str += '<tr><td>' + result[i].name + '</td>' + '<td><button id="editbutton" onclick="editform(this.value,\'' + String(result[i].name) + '\')" value = "' + result[i].id '">Edit</button></td></tr>';
                    }
                    str += "</table>";
                    console.log(str);

                    document.getElementById('listdata').innerHTML = str; //Ini kalo pake JS Native, JS Native sama JQuery sama aja, JQuery itu JS Native yang diperpendek
                    //Ini pake JQuery, sama aja kaya yg di atas, jd kalo pake JQuery gabisa, bisa pake JS Native aja
                    //$('#listdata').innerHTML = str;
                }
            })
        }